//
//  NetworkManagerTests.swift
//  EmpfohlenTests
//
//  Created by Aleksandr Zhovtyi on 12/21/17.
//  Copyright © 2017 Aleksandr Zhovtyi. All rights reserved.
//

import XCTest
@testable import Empfohlen

class NetworkManagerTests: XCTestCase {
    
    let request = URLRequest(url:  URL(string: "https://itunes.apple.com/")!)
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    func testCorrectResponse() {
        // given
        let promise = expectation(description: "Status code: 200")
        var status: Bool = false
        
        // when
        var json = [String: Any]()
        json["status"] = 200
        let data = try! JSONSerialization.data(withJSONObject: json, options: [])
        
        
        
        let response = HTTPURLResponse(url: request.url!, statusCode: 200, httpVersion: nil, headerFields: nil)
        let session = MockURLSession(data: data, response: response, error: nil)
        
        
        let networkManager = NetworkManager(with: session)
        networkManager.execute(request) { result in
            if result.isSuccess {
                status = true
            }
            promise.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
        
        // then
        XCTAssertEqual(status, true, "Didn't parse fake response")
    }
    
    func testStatusMoreThen500ErrorNil () {
        // given
        let promise = expectation(description: "Status code: > 500")
        var errorRecieved: Bool = false
        
        // when
        let response = HTTPURLResponse(url: request.url!, statusCode: 501, httpVersion: nil, headerFields: nil)
        let session = MockURLSession(jsonDict: [:], response: response, error: nil)
        
        
        let networkManager = NetworkManager(with: session!)
        networkManager.execute(request) { result in
            if result.isFailure {
                errorRecieved = true
            }
            promise.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
        
        // then
        XCTAssertEqual(errorRecieved, true, "Didn't parse fake response")
    }
    
    func testErrorStatus () {
        // given
        let promise = expectation(description: "Status code: > 500")
        var errorRecieved: Bool = false
        
        // when
        let response = HTTPURLResponse(url: request.url!, statusCode: 404, httpVersion: nil, headerFields: nil)
        let error = NSError(domain: "", code: NSURLErrorNotConnectedToInternet, userInfo: nil)
        let session = MockURLSession(jsonDict: [:], response: response, error: error)
        
        
        let networkManager = NetworkManager(with: session!)
        networkManager.execute(request) { result in
            if result.isFailure {
                errorRecieved = true
            }
            promise.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
        
        // then
        XCTAssertEqual(errorRecieved, true, "Didn't parse fake response")
    }
    
    func testIncorrectJsonData() {
        // given
        let promise = expectation(description: "Status code: > 500")
        var errorRecieved: Bool = false
        
        // when
        let str = "{\"asdfsdf}"
        let data = str.data(using: .utf8)
        
        
        let response = HTTPURLResponse(url: request.url!, statusCode: 200, httpVersion: nil, headerFields: nil)
        let session = MockURLSession(data: data, response: response, error: nil)
        
        
        let networkManager = NetworkManager(with: session)
        networkManager.execute(request) { result in
            if result.isFailure {
                errorRecieved = true
            }
            promise.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
        
        // then
        XCTAssertEqual(errorRecieved, true, "Didn't parse fake response")
    }
    
}
