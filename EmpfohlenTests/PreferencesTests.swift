//
//  PreferencesTests.swift
//  EmpfohlenTests
//
//  Created by Aleksandr Zhovtyi on 12/7/17.
//  Copyright © 2017 Aleksandr Zhovtyi. All rights reserved.
//

import XCTest
@testable import Empfohlen

class PreferencesTests: XCTestCase {
    
    private var userDefaults: UserDefaults!
    private var preferences: Preferences!
    
    override func setUp() {
        super.setUp()
       
        userDefaults = UserDefaults(suiteName: #file)
        userDefaults.removePersistentDomain(forName: #file)
        
        preferences = Preferences(using: userDefaults)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testClearPreferences() {
        preferences.registerDefaults()
    }
    
}
