//
//  Menu.swift
//  Empfohlen
//
//  Created by Aleksandr Zhovtyi on 12/5/17.
//  Copyright © 2017 Aleksandr Zhovtyi. All rights reserved.
//

import CoreData



// MARK: Properties

extension Menu {
    var contentItems: [[String: String]] {
        get {
            if let str = self.content, let data = str.data(using: .utf8) {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: [])
                    return json as? [[String: String]] ?? []
                    
                } catch let error as NSError {
                    print("⚠️ ", error.debugDescription)
                }
            }
            
            return []
        }
        set {
            do {
                let data = try JSONSerialization.data(withJSONObject: newValue, options: [])
                self.content = String.init(data: data, encoding: .utf8)
            }
            catch let error as NSError {
                print("⚠️ ", error.debugDescription)
                self.content = nil
            }
        }
        
    }
}

extension Menu {
    enum DisplayMode: String, Iteratable {
        case `default`
        case list
        case home
        case itunes
        case link
    }
    
    var displayMode: DisplayMode {
        for val in DisplayMode.hashValues() {
            if type == val.rawValue {
                return val
            }
        }
        return .default
    }
}

// MARK: - Parsing
extension Menu {
    
    static func removeMissing(in json: [[String: Any]], from database: Database = .default) {
        
        let viewContext = database.viewContext
        let moc = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        moc.persistentStoreCoordinator = database.persistentStoreCoordinator
        
        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Menu")
        let ids = json.map{ $0["id"] as? Int32 }.flatMap{ $0 }
        if ids.count > 0 {
            fetch.predicate = NSPredicate(format: "NOT (%K IN %@)", #keyPath(Menu.id), ids)
        }
        
        let request = NSBatchDeleteRequest(fetchRequest: fetch)
        request.resultType = .resultTypeObjectIDs
        
        do {
            let result = try moc.execute(request) as? NSBatchDeleteResult
            if let objectIDArray = result?.result as? [NSManagedObjectID] {
                let changes = [NSDeletedObjectsKey : objectIDArray]
                NSManagedObjectContext.mergeChanges(fromRemoteContextSave: changes, into: [moc, viewContext])
                
                //                moc.refreshAllObjects()
            }
        } catch {
            fatalError("Failed to perform batch update: \(error)")
        }
        
    }
    
    static func parse(_ json: [[String: Any]], into context: NSManagedObjectContext) {
        
        for record in json {
            guard let id = record["id"] as? Int32 else { continue }
            
            let entity = Menu(context: context)
            entity.id = id
            entity.title = record["title"] as? String
            entity.created = record["created"] as? Double ?? Date().timeIntervalSince1970
            entity.lastUpdate = record["updated"] as? Double  ?? Date().timeIntervalSince1970
            entity.order = (record["_order"] as? NSString)?.intValue ?? 0
            entity.content = record["content"] as? String
            if entity.content.isEmptyOrNil, let content = record["content_data"] as? [[String: String]] {
                entity.contentItems = content
            }
            entity.type = record["page_type"] as? String
        }
    }
}
