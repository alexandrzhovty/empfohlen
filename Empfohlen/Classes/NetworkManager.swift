//
//  NetworkManager.swift
//  Empfohlen
//
//  Created by Aleksandr Zhovtyi on 12/8/17.
//  Copyright © 2017 Aleksandr Zhovtyi. All rights reserved.
//

import Foundation

class NetworkManager {
    
    private(set) var session: URLSession
    init(with session: URLSession = .shared) {
        self.session = session
    }
    
    @discardableResult
    func execute(_ request: URLRequest, completion: @escaping (Result<[String: Any]>) -> Void) -> RequestToken {
        NetworkActivity.started()
        let task = session.dataTask(with: request) {
            (data, response, error) in
            
            defer { NetworkActivity.finished() }
            
            guard let response = response as? HTTPURLResponse else { return }
            switch error {
            case .some(let error as NSError) where error.code == NSURLErrorNotConnectedToInternet:
                DispatchQueue.main.async { completion(.failure(error)) }
                
            case .some(let error):
                DispatchQueue.main.async { completion(.failure(error)) }
                
            case .none where 200 ... 399 ~= response.statusCode:
                do {
                    let json = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any] ?? [:]
                    DispatchQueue.main.async { completion(.success(json)) }
                } catch {
                    DispatchQueue.main.async { completion(.failure(error)) }
                }
                
            case .none:
                
                DispatchQueue.main.async {
                    completion(.failure(HTTPURLResponse.localizedString(forStatusCode: response.statusCode)))
                }
            }
            
            
        }
        task.resume()
        
        return RequestToken(task: task)
        
    }
}
