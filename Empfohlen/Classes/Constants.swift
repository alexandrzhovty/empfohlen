//
//  Constants.swift
//  Empfohlen
//
//  Created by Aleksandr Zhovtyi on 12/14/17.
//  Copyright © 2017 Aleksandr Zhovtyi. All rights reserved.
//

import Foundation

struct Constants {
    private init(){}
    
    #if DEBUG
    static let iTunesAppID = "895394516"
    #else
    static let iTunesAppID = "1241984225"
    #endif
}
