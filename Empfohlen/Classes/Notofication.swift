//
//  Notofication.swift
//  Empfohlen
//
//  Created by Aleksandr Zhovtyi on 12/5/17.
//Copyright © 2017 Aleksandr Zhovtyi. All rights reserved.
//

import Foundation

extension Notification.Name {
    
    /// Posted by **AppDelegate** when notification recieved
    ///
    /// The notification object contains **UNNotification**
    ///
    public static let didReceiveRemoteNotification = Notification.Name(rawValue: "\(Bundle.main.bundleIdentifier!).didReceiveRemoteNotification")
    
    
}
