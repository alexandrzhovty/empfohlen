//
//  ParseOperation.swift
//  Empfohlen
//
//  Created by Aleksandr Zhovtyi on 12/5/17.
//  Copyright © 2017 Aleksandr Zhovtyi. All rights reserved.
//

import CoreData

class ParseOperation: Operation {
    let viewContext: NSManagedObjectContext
    let parseContext: NSManagedObjectContext
    
    
    typealias ParsingHandler = (_ context: NSManagedObjectContext) -> Void
    
    var parsingHandler: ParsingHandler
    
    init(for database: Database = .default, parsingHandler: @escaping ParsingHandler) {
        self.parsingHandler = parsingHandler
        viewContext = database.viewContext
        parseContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        super.init()
        
        parseContext.persistentStoreCoordinator = database.persistentStoreCoordinator
        parseContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(managedObjectContextDidSave(_:)), name: .NSManagedObjectContextDidSave, object: parseContext)
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func main() {
        parseContext.performAndWait {
            
            self.parsingHandler(self.parseContext)
            
            if self.parseContext.hasChanges {
                do { try self.parseContext.save() }
                catch { print(error.localizedDescription) }
            }
        }
    }
    
    @objc func managedObjectContextDidSave(_ notification: Notification) {
        viewContext.performAndWait {
            viewContext.mergeChanges(fromContextDidSave: notification)
        }
    }
}
