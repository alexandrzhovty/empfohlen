//
//  Database.swift
//  Empfohlen
//
//  Created by Aleksandr Zhovtyi on 12/7/17.
//Copyright © 2017 Aleksandr Zhovtyi. All rights reserved.
//

import CoreData

class Database {
    static let `default` = Database()
    private(set) var modelName: String
    
    /// Creates a container using the model named `name` in the main bundle
    ///
    /// - Parameter modelName: model name
    init(modelName: String = Bundle.main.infoDictionary!["CFBundleName"] as! String) {
        self.modelName = modelName
    }
    
    // MARK:  Core Data stack
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: modelName)
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    
}

extension Database {
    var viewContext: NSManagedObjectContext {
        return persistentContainer.viewContext
    }
    
    var persistentStoreCoordinator: NSPersistentStoreCoordinator {
        return persistentContainer.persistentStoreCoordinator
    }
}
