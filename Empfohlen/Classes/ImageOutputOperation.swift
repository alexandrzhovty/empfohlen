//
//  File.swift
//  Famous
//
//  Created by Aleksandr Zhovtyi on 10/29/17.
//  Copyright © 2017 Empfohlen. All rights reserved.
//

import Foundation


final class ImageOutputOperation: ImageTakeOperation {
    
    private let completion: (Image?) -> ()
    
    init(completion: @escaping (Image?) -> ()) {
        self.completion = completion
        super.init(image: nil)
    }
    
    override public func main() {
        if isCancelled { completion(nil)}
        completion(inputImage)
    }
}
