//
//  ImagePass.swift
//  Famous
//
//  Created by Aleksandr Zhovtyi on 10/27/17.
//  Copyright © 2017 Empfohlen. All rights reserved.
//

import UIKit

protocol ImagePass {
    var image: UIImage? { get }
}
