//
//  BrowserController.swift
//  Empfohlen
//
//  Created by Aleksandr Zhovtyi on 11/30/17.
//Copyright © 2017 Aleksandr Zhovtyi. All rights reserved.
//

import UIKit
import WebKit






final class BrowserController: UIViewController {
    //    MARK: - Properties & variables
    //    MARK: Public
    var url: URL? {
        didSet {
            guard let url = self.url else { return }
            self.displayMode = .url(url)
            self.configure(webView)
        }
    }
    
    
    //    MARK: Outlets
    var progressView: UIProgressView
//    @IBOutlet var backButton: UIBarButtonItem!
    
    
    //    MARK: Private
    fileprivate var webView: WKWebView!
    fileprivate var bottomView: BottomView
    
    fileprivate var observerWasCreated = false
    
    //    MARK: Enums & Structures
    private var displayMode: DisplayMode?
    private enum DisplayMode {
        case url(URL)
        case menu(Menu)
        case post(Post)
    }
    
    
    init() {
        self.progressView = UIProgressView(progressViewStyle: .default)
        self.bottomView = BottomView.fromNib()
        super.init(nibName:  nil, bundle: nil)
    }
    
    convenience init(url: URL) {
        self.init()
        self.displayMode = .url(url)
    }
    
    convenience init(with menu: Menu) {
        self.init()
        self.displayMode = .menu(menu)
    }
    
    convenience init(with post: Post) {
        self.init()
        self.displayMode = .post(post)
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //    MARK: - View life cycle
    deinit {
        unregisterObserver()
    }
    
    
    override func loadView() {
        super.loadView()
        
        Appearance.customize(viewController: self)
        
        view.addSubview(bottomView)
        
        let item = UIBarButtonItem(image: UIImage(named: "Icon-Menu"), style: .plain, target: self, action: #selector(didTapMenu(_:)))
        self.navigationItem.rightBarButtonItem = item
        
        webView = WKWebView()
        view.addSubview(webView)
        
        progressView = UIProgressView(progressViewStyle: .default)
        view.addSubview(progressView)
        
        view.subviews.forEach{ setup($0) }

        registerObserver()
        
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
        
        
        if webView.url == nil {
            configure(webView)
        }
        
    }
}

// MARK: - Public
extension BrowserController {
    func set(_ menu: Menu) {
        self.displayMode = .menu(menu)
        self.configure(webView)
    }
}

//    MARK: - Utilities
extension BrowserController  {
    fileprivate func setup(_ viewToSetup: UIView) {
        switch viewToSetup {
            
        case bottomView:
            bottomView.translatesAutoresizingMaskIntoConstraints = false
            bottomView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
            bottomView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
            view.bottomAnchor.constraint(equalTo: bottomView.bottomAnchor).isActive = true
            
        case webView:
            webView.translatesAutoresizingMaskIntoConstraints = false
            
            webView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
            webView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
            webView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
            webView.bottomAnchor.constraint(equalTo: bottomView.topAnchor).isActive = true
            
            webView.navigationDelegate = self
            
        case progressView:
            progressView.translatesAutoresizingMaskIntoConstraints = false
            
            progressView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
            progressView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
            progressView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
            
            progressView.progress = 0
            progressView.isHidden = true
            
        default: break
        }
        
    }
    
    private func configure(_ viewToConfig: UIView) {
        switch viewToConfig {
        case webView:
            guard let displayMode = self.displayMode else { break }
            
            switch displayMode {
            case .url(let url):
                webView.load(URLRequest(url: url))
                
            case .menu(let menu):
                
                let template: String
                do { template = try String(contentsOfFile: Resources.HTML.template.path) }
                catch let error as NSError {
                    print("⚠️ ", String(describing: type(of: self)),":", #function, " cannot load template ", error.debugDescription)
                    return
                }
                
                var str = template.replacingOccurrences(of: "$$$TITLE$$$", with: menu.title ?? "")
                str = str.replacingOccurrences(of: "$$$BODY$$$", with: menu.content ?? "")
                
                 let bundlePath = Bundle.main.bundleURL
                webView.loadHTMLString(str, baseURL: bundlePath)
                
            case .post(let post):
                let template: String
                do { template = try String(contentsOfFile: Resources.HTML.post.path) }
                catch let error as NSError {
                    print("⚠️ ", String(describing: type(of: self)),":", #function, " cannot load template ", error.debugDescription)
                    return
                }
                
                var str = template.replacingOccurrences(of: "$$$TITLE$$$", with: post.title ?? "")
                if let sURL  = post.cover {
                    str = str.replacingOccurrences(of: "$$$COVER$$$", with: sURL)
                    str = str.replacingOccurrences(of: "$$$HEADERSTYLE$$$", with: "")
                } else {
                    str = str.replacingOccurrences(of: "$$$COVER$$$", with: "")
                    str = str.replacingOccurrences(of: "$$$HEADERSTYLE$$$", with: "style=\"position: relative\"")
                }
                
                
                
                
                var body = ""
                
                for row in post.contentItems {
                    guard
                        let type = row["type"] as? String,
                        let content = row["content"] as? String
                    else { continue }
                    
                    switch type {
                    case "title":
                        body.append("<h2>\(content)</h2>")
                    case "short_text":
                        body.append("<p class=short>\(content)</p>")
                    case "text":
                        body.append("<p>\(content)</p>")
                    case "image":
                        body.append("<img src=\"\(content)\" width=\"100%\"")
                    default: break
                    }
                }
                
                str = str.replacingOccurrences(of: "$$$BODY$$$", with: body)
                
                let bundlePath = Bundle.main.bundleURL
                webView.loadHTMLString(str, baseURL: bundlePath)
            }
            
        default: break
        }
    }
}


//    MARK: - Observer
extension BrowserController: ObserverProtocol {
    func registerObserver() {
        webView.addObserver(self, forKeyPath: "estimatedProgress", options: .new, context: nil)
//        webView.addObserver(self, forKeyPath: "canGoBack", options: .new, context: nil)
        observerWasCreated = true
    }
    
    func unregisterObserver() {
        if observerWasCreated {
            webView.removeObserver(self, forKeyPath: "estimatedProgress")
//            webView.removeObserver(self, forKeyPath: "canGoBack")
        }
    }
    
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        switch keyPath {
        case "estimatedProgress"?:
            self.progressView.isHidden = self.webView.estimatedProgress == 1
            self.progressView.progress = Float(webView.estimatedProgress)
            
            //        case "canGoBack"?:
            //            navigationItem.leftBarButtonItem = webView.canGoBack ? backButton : nil
            
        default:
            return super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
        }
    }
}

extension BrowserController {
    @IBAction func didTapMenu(_ button: Any) {
        let vc = Storyboard.menu.instantiateViewController(MenuController.self)
        present(vc, animated: true, completion: nil)
    }
}

//    MARK: - Web view protocols
extension BrowserController: WKNavigationDelegate {
    //    MARK: Navigation delegate
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        progressView.isHidden = false
        NetworkActivity.started()
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        progressView.isHidden = true
        progressView.progress = 0.0
        NetworkActivity.finished()
    }

    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        NetworkActivity.finished()
    }

    // Recognize an Apple Maps URL. This will trigger the native app. But only if a search query is present. Otherwise
    // it could just be a visit to a regular page on maps.apple.com.
    fileprivate func isAppleMapsURL(_ url: URL) -> Bool {
        if url.scheme == "http" || url.scheme == "https" {
            if url.host == "maps.apple.com" && url.query != nil {
                return true
            }
        }
        return false
    }

    // Recognize a iTunes Store URL. These all trigger the native apps. Note that appstore.com and phobos.apple.com
    // used to be in this list. I have removed them because they now redirect to itunes.apple.com. If we special case
    // them then iOS will actually first open Safari, which then redirects to the app store. This works but it will
    // leave a 'Back to Safari' button in the status bar, which we do not want.
    fileprivate func isStoreURL(_ url: URL) -> Bool {
        if url.scheme == "http" || url.scheme == "https" {
            if url.host == "itunes.apple.com" {
                return true
            }
        }
        return false
    }

    // Mark: User Agent Spoofing

    fileprivate func resetSpoofedUserAgentIfRequired(_ webView: WKWebView, newURL: URL) {
        // Reset the UA when a different domain is being loaded
        //        if webView.url?.host != newURL.host {
        //            webView.customUserAgent = nil
        //        }
    }

    fileprivate func restoreSpoofedUserAgentIfRequired(_ webView: WKWebView, newRequest: URLRequest) {
        // Restore any non-default UA from the request's header
        //        let ua = newRequest.value(forHTTPHeaderField: "User-Agent")
        //        webView.customUserAgent = ua != UserAgent.defaultUserAgent() ? ua : nil
    }

    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {

//        print("👍 ", String(describing: type(of: self)),":", #function)
        
        guard let url = navigationAction.request.url else {
            decisionHandler(.cancel)
            return
        }

        if url.scheme == "about" {
            decisionHandler(WKNavigationActionPolicy.allow)
            return
        }

         let app = UIApplication.shared

        if url.scheme == "tel" || url.scheme == "facetime" || url.scheme == "facetime-audio" {
            if app.canOpenURL(url) {
                app.open(url, options: [:], completionHandler: nil)
                decisionHandler(WKNavigationActionPolicy.cancel)
                return
            }
        }

        // Second special case are a set of URLs that look like regular http links, but should be handed over to iOS
        // instead of being loaded in the webview. Note that there is no point in calling canOpenURL() here, because
        // iOS will always say yes. TODO Is this the same as isWhitelisted?

        if isAppleMapsURL(url) || isStoreURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
            decisionHandler(WKNavigationActionPolicy.cancel)
            return
        }

        // Handles custom mailto URL schemes.
        if url.scheme == "mailto" {
            if app.canOpenURL(url) {
                app.open(url, options: [:], completionHandler: nil)
                decisionHandler(WKNavigationActionPolicy.cancel)
                return
            }
        }

        // This is the normal case, opening a http or https url, which we handle by loading them in this WKWebView. We
        // always allow this. Additionally, data URIs are also handled just like normal web pages.
        if url.scheme == "http" || url.scheme == "https" || url.scheme == "data" {
            if navigationAction.navigationType == .linkActivated {
                resetSpoofedUserAgentIfRequired(webView, newURL: url)
            } else if navigationAction.navigationType == .backForward {
                restoreSpoofedUserAgentIfRequired(webView, newRequest: navigationAction.request)
            }
            decisionHandler(WKNavigationActionPolicy.allow)
            return
        }

        

        decisionHandler(WKNavigationActionPolicy.allow)
    }
}

