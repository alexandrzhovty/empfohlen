//
//  ImageLocalOperation.swift
//  Famous
//
//  Created by Aleksandr Zhovtyi on 10/27/17.
//  Copyright © 2017 Empfohlen. All rights reserved.
//

import Foundation

class ImageLocalOperation: AsyncOperation {
    private var url: URL
    fileprivate var outputImage: Image?
    
    public init(url: URL) {
        self.url = url
        super.init()
    }
    
    override public func main() {
        if self.isCancelled { return }
        let location = FileManager.cachedImagesDirectory.appendingPathComponent(url.cacheKey as String)
        DispatchQueue.global(qos: .background).async {
            do {
                let imageData = try Data(contentsOf: location)
                self.outputImage = Image(data: imageData)
                ImageCache.default.set(self.outputImage, forKey: self.url)
            }
            catch {
                 self.outputImage = nil
            }
            
            if self.isCancelled { return }
            self.state = .finished
        }
    }
}


extension ImageLocalOperation: ImagePass {
    var image: Image? {
        return outputImage
    }
}
