//
//  RequestToken.swift
//  Empfohlen
//
//  Created by Aleksandr Zhovtyi on 12/4/17.
//  Copyright © 2017 Aleksandr Zhovtyi. All rights reserved.
//

import Foundation

class RequestToken {
    private weak var task: URLSessionTask?
    
    
    init(task: URLSessionTask) {
        self.task = task
    }
    
    func cancel() {
        task?.cancel()
    }
}
