//
//  FileManager.swift
//  Empfohlen
//
//  Created by Aleksandr Zhovtyi on 12/5/17.
//  Copyright © 2017 Aleksandr Zhovtyi. All rights reserved.
//

import Foundation

extension FileManager {
    private static let cacheDirectoryName: String = Bundle.main.bundleIdentifier! + ".images"
    static  var cachedImagesDirectory: URL {
        return FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first!.appendingPathComponent(cacheDirectoryName)
    }
    
    /// Creates dictrectoy for cached images if is neccessary
    static func createDirectoryForCachedImages() {
        let url = FileManager.cachedImagesDirectory
        var isDir : ObjCBool = false
        if FileManager.default.fileExists(atPath: url.path, isDirectory: &isDir) == false {
            do { try FileManager.default.createDirectory(at: url, withIntermediateDirectories: false, attributes: nil) }
            catch let error as NSError {
                NSLog("Unable to create directory \(error.debugDescription)")
            }
        }
    }
}
