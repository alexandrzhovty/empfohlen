//
//  PostListTopBannerView.swift
//  Empfohlen
//
//  Created by Aleksandr Zhovtyi on 12/1/17.
//  Copyright © 2017 Aleksandr Zhovtyi. All rights reserved.
//

import UIKit
import SafariServices

class PostListTopBannerView: UIView {

    @IBOutlet weak fileprivate var imageView: UIImageView!
    @IBOutlet weak fileprivate var titleLabel: UILabel!
    @IBOutlet weak fileprivate var button: UIButton!
    
    private var imageProvider: ImageProvider?
    private var preferences: Preferences = .default
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imageView.image = nil
        subviews.forEach(configure)
    }
    
    deinit {
        imageProvider?.cancel()
        imageProvider = nil
    }

}

// MARK: - Utilites
extension PostListTopBannerView {
    
    
   
    
    private func configure(_ viewToConfig: UIView) {
        
        if Thread.isMainThread == false {
            DispatchQueue.main.async {
                self.configure(viewToConfig)
            }
            return
        }
        
        switch viewToConfig {
        case button:
            button.setTitle(preferences.contentButtonTitle, for: .normal)
            
        case titleLabel:
            titleLabel.text = preferences.contentText
            
        case imageView:
            if let url = preferences.contentLogoUrl {
                let imageProvider = ImageProvider(imageURL: url) {
                    [weak self] image in
                    guard let `self` = self else { return }
                    
                    OperationQueue.main.addOperation {
                        self.imageView.image = image
                    }
                    
                    
                }
                self.imageProvider = imageProvider
            } else {
                self.imageView.image = nil
            }
            
        default: break
        }
    }
    
}

// MARK: - Outlets
extension PostListTopBannerView {
    @IBAction fileprivate func didTapButton(_ sender: UIButton) {
        guard let url = preferences.contentButtonUrl else {
            return
        }
        
        let vc = self.viewController()!
        let svc = SFSafariViewController(url: url)
        vc.present(svc, animated: true, completion: nil)
    }
}
