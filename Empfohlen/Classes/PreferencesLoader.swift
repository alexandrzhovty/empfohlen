//
//  OptionsLoadeer.swift
//  Empfohlen
//
//  Created by Aleksandr Zhovtyi on 12/4/17.
//  Copyright © 2017 Aleksandr Zhovtyi. All rights reserved.
//

import Foundation

final class PreferencesLoader {
    private(set) var networkManager: NetworkManager
    private(set) var preferences: Preferences
    
    init(for preferences: Preferences = .default, networkManager: NetworkManager = .init()) {
        self.networkManager = networkManager
        self.preferences = preferences
    }
    
    @discardableResult
    func load(completion: @escaping (Error?) -> Void ) -> RequestToken {
        let request = Endpoint.preferences.request
        
        return networkManager.execute(request, completion: {
            [weak self] result in
            
            guard result.isSuccess else {
                completion(result.error)
                return
            }
            
            if let json = result.value?["data"] as? [String: Any] {
                self?.preferences.parse(json)
                completion(nil)
            } else {
                print("⚠️ ", String(describing: type(of: self)),":", #function, " Incorrect JSON ")
            }
        })
    }
    
}
