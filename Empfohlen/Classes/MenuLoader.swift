//
//  MenuLoader.swift
//  Empfohlen
//
//  Created by Aleksandr Zhovtyi on 12/5/17.
//  Copyright © 2017 Aleksandr Zhovtyi. All rights reserved.
//

import Foundation


final class MenuLoader {
    private(set) var networkManager: NetworkManager
    fileprivate let operationQueue: OperationQueue
    
    init(networkManager: NetworkManager = .init()) {
        self.networkManager = networkManager
        self.operationQueue = OperationQueue()
        self.operationQueue.maxConcurrentOperationCount = 1
    }
    
    @discardableResult
    func load(page: Int = 1, completion: @escaping (Error?) -> Void ) -> RequestToken {
        let request = Endpoint.menu(page: 1).request
       
        return networkManager.execute(request, completion: {
            [weak self] result in
            guard let `self` = self else { return }
            
            guard result.isSuccess else {
                completion(result.error)
                return
            }
            
            if let json = result.value?["data"] as? [[String: Any]] {
                if page == 1 {
                    Menu.removeMissing(in: json)
                }
                
                self.operationQueue.addOperation(
                    ParseOperation(parsingHandler: { context in
                        Menu.parse(json, into: context)
                    })
                )
            } else {
                print("⚠️ ", String(describing: type(of: self)),":", #function, " Incorrect JSON ")
            }
            
            completion(nil)
            
        })
    }
    
}
