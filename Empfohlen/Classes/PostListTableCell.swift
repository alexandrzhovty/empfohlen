//
//  PostListTableCell.swift
//  Empfohlen
//
//  Created by Aleksandr Zhovtyi on 12/1/17.
//  Copyright © 2017 Aleksandr Zhovtyi. All rights reserved.
//

import UIKit

class PostListTableCell: UITableViewCell {
    
    @IBOutlet weak fileprivate var backgroundImageView: UIImageView!
    @IBOutlet weak fileprivate var postImageView: UIImageView!
    @IBOutlet weak fileprivate var bodyLabel: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let view = UIView()
        view.backgroundColor = .clear
        selectedBackgroundView = view
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        postImageView.image = nil
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    

}

extension PostListTableCell {
    var postImage: UIImage? {
        get { return postImageView.image }
        set { postImageView.setImage(newValue, animated: true) }
    }
    
    var body: String? {
        get { return bodyLabel.text }
        set { bodyLabel.text = newValue }
    }
}
