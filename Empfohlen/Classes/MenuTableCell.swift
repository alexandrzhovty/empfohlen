//
//  MenuTableCell.swift
//  Empfohlen
//
//  Created by Aleksandr Zhovtyi on 11/30/17.
//  Copyright © 2017 Aleksandr Zhovtyi. All rights reserved.
//

import UIKit

final class MenuTableCell: UITableViewCell {
    
    @IBOutlet weak fileprivate var titleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        backgroundColor = .clear
        contentView.backgroundColor = .clear
        
        selectedBackgroundView = {
            let view = UIView()
            view.backgroundColor = Color(rgbValue: 0xcac7c6).withAlphaComponent(0.25)
            return view
        }()
    }

}

// MARK: - Interface
extension MenuTableCell {
    var title: String? {
        get { return titleLabel.text }
        set { titleLabel.text = newValue }
    }
}
