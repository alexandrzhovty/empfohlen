//
//  TitleView.swift
//  Empfohlen
//
//  Created by Aleksandr Zhovtyi on 11/29/17.
//  Copyright © 2017 Aleksandr Zhovtyi. All rights reserved.
//

import UIKit

class NavigationTitleView: UIView {
    // MARK: Variabales
    private(set) var imageView: UIImageView!
    private(set) var titleLabel: UILabel!
    private var preferences: Preferences = .default
    private var imageProvider: ImageProvider?
    
    // Initliazation
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
        
    }
    
    deinit {
//        print("👍 ", String(describing: type(of: self)),":", #function)
        unregisterObserver()
        imageProvider?.cancel()
        imageProvider = nil
    }
    
}



// MARK: - Observers
extension NavigationTitleView: ObserverProtocol {
    static private var observationContext = arc4random()
    
    func registerObserver() {
        let userDefaults = preferences.userDefaults
        userDefaults.addObserver(self, forKeyPath: #keyPath(Preferences.headerText), options: [.new], context: &NavigationTitleView.observationContext)
        userDefaults.addObserver(self, forKeyPath: #keyPath(Preferences.appLogo), options: [.new], context: &NavigationTitleView.observationContext)
    }
    
    func unregisterObserver() {
        let userDefaults = preferences.userDefaults
        userDefaults.removeObserver(self, forKeyPath: #keyPath(Preferences.headerText))
        userDefaults.removeObserver(self, forKeyPath: #keyPath(Preferences.appLogo))
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        
        guard context == &NavigationTitleView.observationContext, let kp = keyPath else {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
            return
        }
        
        switch kp {
        case #keyPath(Preferences.headerText):
            configure(titleLabel)
        case #keyPath(Preferences.appLogo):
            configure(imageView)
        default: break
        }
    }
}

// MARK: - Utilites
extension NavigationTitleView {
    private func commonInit() {
        imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        self.addSubview(imageView)
        
        
        titleLabel = UILabel()
        titleLabel.font = Font.OpenSans.regular.font(size: 12)
        titleLabel.textColor = .white
//        titleLabel.text = NSLocalizedString("Willkommen in der empfohlen.de-App", comment: "")
        self.addSubview(titleLabel)
        
        
        configure(titleLabel)
        configure(imageView)
        
        subviews.forEach{ setup($0) }
        
        layoutIfNeeded()
        
        registerObserver()
    }
    
    private func setup(_ viewToSetup: UIView) {
        switch viewToSetup {
        case imageView:
            imageView.contentMode = .scaleAspectFit
            imageView.translatesAutoresizingMaskIntoConstraints = false
            imageView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
            imageView.widthAnchor.constraint(equalToConstant: 250).isActive = true
            imageView.heightAnchor.constraint(equalToConstant: 20).isActive = true
            
            if #available(iOS 11, *) {
                imageView.topAnchor.constraint(equalTo: topAnchor).isActive = true
            } else {
                imageView.topAnchor.constraint(equalTo: topAnchor, constant: -17).isActive = true
            }
            
            
            
            
            
//            imageView.leadingAnchor.constraint(greaterThanOrEqualTo: leadingAnchor, constant: 0).isActive = true
//            trailingAnchor.constraint(lessThanOrEqualTo: imageView.trailingAnchor, constant: 0).isActive = true
            
            //            bottomAnchor.constraint(equalTo: imageView.bottomAnchor).isActive = true
            
        case titleLabel:
            titleLabel.translatesAutoresizingMaskIntoConstraints = false
//
            titleLabel.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
////
            titleLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor).isActive = true
            titleLabel.heightAnchor.constraint(equalToConstant: 16).isActive = true
//
////            titleLabel.leadingAnchor.constraint(greaterThanOrEqualTo: imageView.leadingAnchor, constant: 0).isActive = true
////            imageView.trailingAnchor.constraint(lessThanOrEqualTo: titleLabel.trailingAnchor, constant: 0).isActive = true
//
//            bottomAnchor.constraint(equalTo: titleLabel.bottomAnchor).isActive = true
            
            
        default: break
        }
    }
    
    private func configure(_ viewToConfig: UIView) {
        
        if Thread.isMainThread == false {
            DispatchQueue.main.async {
                self.configure(viewToConfig)
            }
            return
        }
        
        switch viewToConfig {
        case titleLabel:
            titleLabel.text = preferences.headerText
            
        case imageView:
            if let url = preferences.appLogo {
                let imageProvider = ImageProvider(imageURL: url) {
                    [weak self] image in
                    guard let `self` = self else { return }
                    
                    OperationQueue.main.addOperation {
                        self.imageView.image = image
                    }
                    
                    
                }
                self.imageProvider = imageProvider
            }
            
        default: break
        }
    }
    
}
