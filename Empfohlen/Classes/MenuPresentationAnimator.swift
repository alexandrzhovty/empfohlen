//
//  MenuPresentationAnimator.swift
//  Empfohlen
//
//  Created by Aleksandr Zhovtyi on 11/30/17.
//  Copyright © 2017 Aleksandr Zhovtyi. All rights reserved.
//

import UIKit

class MenuPresentationAnimator: NSObject {
    var isPresentation : Bool = false
}



extension MenuPresentationAnimator: UIViewControllerAnimatedTransitioning {
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.25
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let fromVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from) else { return }
        guard let toVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to) else { return }
        guard let fromView = fromVC.view else { return }
        guard let toView = toVC.view else { return }
        let containerView = transitionContext.containerView
        
        var viewToAnimate: UIView
        var endFrame: CGRect
        
        let shadowView = UIView()
        
        if isPresentation {
            viewToAnimate = toView
            endFrame = transitionContext.finalFrame(for: toVC)
            viewToAnimate.frame.origin.x = fromView.frame.width
            endFrame.origin.x = fromView.frame.width - endFrame.width
            containerView.addSubview(viewToAnimate)
            
            
            shadowView.backgroundColor = UIColor.lightGray
            
            shadowView.frame = viewToAnimate.frame
            shadowView.frame.origin.x = 0
            shadowView.frame.size.width = 0.4
            
            viewToAnimate.addSubview(shadowView)
        
            shadowView.layer.shadowColor = UIColor.black.cgColor
            shadowView.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
            shadowView.layer.shadowOpacity = 0.99
            shadowView.layer.shadowRadius = 5.0
            
            shadowView.clipsToBounds = false
            
        } else {
            viewToAnimate = fromView
            endFrame = viewToAnimate.frame
            endFrame.origin.x = toView.frame.width
        }
        
        
        let duration = self.transitionDuration(using: transitionContext)
        UIView.setAnimationCurve(UIViewAnimationCurve.easeInOut)
        
        UIView.setAnimationCurve(.easeInOut)
        UIView.animateKeyframes(withDuration: duration, delay: 0, options: [], animations: {
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 1/2) {
                viewToAnimate.frame = endFrame
                
                if self.isPresentation {
                    fromView.frame.origin.x = endFrame.width *  (-1)
                } else {
                    toView.frame.origin.x = 0
                }
            }
            
        }) { finished in
            
            if !transitionContext.transitionWasCancelled, !self.isPresentation {
                if (transitionContext.transitionWasCancelled) {
                    toView.removeFromSuperview()
                } else {
                    if self.isPresentation {
                        fromView.removeFromSuperview()
                    }
                }
            }
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }
        
    }
    
}
