//
//  MenuFooterView.swift
//  Empfohlen
//
//  Created by Aleksandr Zhovtyi on 12/5/17.
//  Copyright © 2017 Aleksandr Zhovtyi. All rights reserved.
//

import UIKit
import SafariServices

class MenuFooterView: UIView {
    
    @IBOutlet weak fileprivate var imageView: UIImageView!
    @IBOutlet weak fileprivate var titleLabel: UILabel!
    @IBOutlet weak fileprivate var subtitleLabel: UILabel!
    @IBOutlet weak fileprivate var button: UIButton!
    
    fileprivate var imageProvider: ImageProvider?
    fileprivate var preferences: Preferences = .default

    override func awakeFromNib() {
        super.awakeFromNib()
        imageView.image = nil
        subviews.forEach{ configure($0) }
    }
    
    deinit {
        imageProvider?.cancel()
        imageProvider = nil
    }

}


// MARK: - Utilites
extension MenuFooterView {
    private func configure(_ viewToConfig: UIView) {
        
        if Thread.isMainThread == false {
            DispatchQueue.main.async {
                self.configure(viewToConfig)
            }
            return
        }
        
        switch viewToConfig {
        case button:
            button.setTitle(preferences.rightSidebarButtonTitle, for: .normal)
            
        case titleLabel:
            titleLabel.text = preferences.rightSidebarTitle
            
        case subtitleLabel:
            subtitleLabel.text = preferences.rightSidebarSubtitle
            
        case imageView:
            if let url = preferences.rightSidebarLogo {
                let imageProvider = ImageProvider(imageURL: url) {
                    [weak self] image in
                    guard let `self` = self else { return }
                    
                    OperationQueue.main.addOperation {
                        self.imageView.image = image
                    }
                    
                    
                }
                self.imageProvider = imageProvider
            } else {
                self.imageView.image = nil
            }
            
        default: break
        }
    }
    
}

// MARK: - Outlets
extension MenuFooterView {
    @IBAction fileprivate func didTapButton(_ sender: UIButton) {
        guard let url = preferences.rightSidebarButtonURL else { return }
        
        self.viewController()?.dismiss(animated: false, completion: {
            guard let vc = AppDelegate.shared.window?.rootViewController else { return }
            let svc = SFSafariViewController(url: url)
            vc.present(svc, animated: true, completion: nil)
        })
        
        
    }
}
