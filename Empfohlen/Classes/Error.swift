//
//  Error.swift
//  Empfohlen
//
//  Created by Aleksandr Zhovtyi on 12/7/17.
//  Copyright © 2017 Aleksandr Zhovtyi. All rights reserved.
//


import Foundation

///
/// Then you can just throw a string: `throw "Some Error"`
///
extension String: Error {}

extension String: LocalizedError {
    public var errorDescription: String? { return self }
}
