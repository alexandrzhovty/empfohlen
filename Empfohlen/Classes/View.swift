//
//  View.swift
//  Empfohlen
//
//  Created by Aleksandr Zhovtyi on 11/30/17.
//Copyright © 2017 Aleksandr Zhovtyi. All rights reserved.
//

import UIKit

extension UIView {
    
    /// Returns the UIViewController object that manages the receiver.
    ///
    /// - Returns: UIViewController
    public func viewController() -> UIViewController? {
        
        var nextResponder: UIResponder? = self
        
        repeat {
            nextResponder = nextResponder?.next
            
            if let viewController = nextResponder as? UIViewController {
                return viewController
            }
            
        } while nextResponder != nil
        
        return nil
    }
    
    public func addConstraint(to view: UIView, margin: CGFloat = 0) {
        
        view.translatesAutoresizingMaskIntoConstraints = false
        
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: margin).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: margin).isActive = true
        view.topAnchor.constraint(equalTo: self.topAnchor, constant: margin).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: margin).isActive = true
    }
    
    
    func captureScreen() -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, UIScreen.main.scale)
        self.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
}
