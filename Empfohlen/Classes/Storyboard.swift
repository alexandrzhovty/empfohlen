//
//  Storyboard.swift
//  Empfohlen
//
//  Created by Aleksandr Zhovtyi on 11/29/17.
//Copyright © 2017 Aleksandr Zhovtyi. All rights reserved.
//

import UIKit


typealias Storyboard = UIStoryboard

enum StroyboadType: String, Iteratable {
    case main
    case menu
    case posts
    case list
    
    var filename: String { return rawValue.capitalized }
}

extension UIStoryboard {
    static let main = Storyboard(.main)
    static let menu = Storyboard(.menu)
    static let posts = Storyboard(.posts)
    static let list = Storyboard(.list)
}


extension UIStoryboard {
    convenience init(_ storyboard: StroyboadType) {
        self.init(name: storyboard.filename, bundle: nil)
    }
    
    /// Instantiates and returns the view controller with the specified identifier.
    ///
    /// - Parameter identifier: uniquely identifies equals to Class name
    /// - Returns: The view controller corresponding to the specified identifier string. If no view controller is associated with the string, this method throws an exception.
    public func instantiateViewController<T>(_ identifier: T.Type) -> T where T: UIViewController {
        let className = String(describing: identifier)
        guard let vc =  self.instantiateViewController(withIdentifier: className) as? T else {
            fatalError("Cannot find controller with identifier \(className)")
        }
        return vc
    }
}
