//
//  InteractorTransition.swift
//  Empfohlen
//
//  Created by Aleksandr Zhovtyi on 11/30/17.
//  Copyright © 2017 Aleksandr Zhovtyi. All rights reserved.
//

import UIKit

class InteractorTransition: UIPercentDrivenInteractiveTransition {
    var hasStarted = false
    var shouldFinish = false
}
