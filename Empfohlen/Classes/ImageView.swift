//
//  ImageView.swift
//  Empfohlen
//
//  Created by Aleksandr Zhovtyi on 12/5/17.
//  Copyright © 2017 Aleksandr Zhovtyi. All rights reserved.
//

import UIKit

extension UIImageView {
    func setImage(_ image: UIImage?, animated: Bool) {
        if animated {
            UIView.transition(
                with: self,
                duration: 0.25,
                options: .transitionCrossDissolve,
                animations: { self.image = image },
                completion: nil
            )
        } else {
            self.image = image
        }
        
    }
}
