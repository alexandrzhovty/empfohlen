//
//  String.swift
//  Empfohlen
//
//  Created by Aleksandr Zhovtyi on 12/5/17.
//  Copyright © 2017 Aleksandr Zhovtyi. All rights reserved.
//

import Foundation

protocol OptionalString {}
extension String: OptionalString {}

extension Optional where Wrapped: OptionalString {
    var isEmptyOrNil: Bool {
        return ((self as? String) ?? "").isEmpty
    }
}


//extension Optional where Wrapped == OptionalString {
//    var isNilOrEmpty: Bool {
//        switch self {
//        case let string?:
//            return string.isEmpty
//        case nil:
//            return true
//        }
//    }
//}
//
//// Since strings are now Collections in Swift 4, you can even
//// add this property to all optional collections:
//
//extension Optional where Wrapped: Collection {
//    var isNilOrEmpty: Bool {
//        switch self {
//        case let collection?:
//            return collection.isEmpty
//        case nil:
//            return true
//        }
//    }
//}

