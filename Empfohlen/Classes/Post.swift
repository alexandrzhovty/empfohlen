//
//  Post.swift
//  Empfohlen
//
//  Created by Aleksandr Zhovtyi on 12/5/17.
//  Copyright © 2017 Aleksandr Zhovtyi. All rights reserved.
//

import CoreData

extension Post {
    
    static func removeMissing(in json: [[String: Any]], from database: Database = .default) {
        
        let viewContext = database.viewContext
        let moc = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        moc.persistentStoreCoordinator = database.persistentStoreCoordinator
        
        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Post")
        let ids = json.map{ $0["id"] as? Int32 }.flatMap{ $0 }
        if ids.count > 0 {
            fetch.predicate = NSPredicate(format: "NOT (%K IN %@)", #keyPath(Menu.id), ids)
        }
        
        let request = NSBatchDeleteRequest(fetchRequest: fetch)
        request.resultType = .resultTypeObjectIDs
        
        do {
            let result = try moc.execute(request) as? NSBatchDeleteResult
            if let objectIDArray = result?.result as? [NSManagedObjectID] {
                let changes = [NSDeletedObjectsKey : objectIDArray]
                NSManagedObjectContext.mergeChanges(fromRemoteContextSave: changes, into: [moc, viewContext])
                
                //                moc.refreshAllObjects()
            }
        } catch {
            fatalError("Failed to perform batch update: \(error)")
        }
        
    }
    
    static func parse(_ json: [[String: Any]], into context: NSManagedObjectContext) {
        for record in json {
            guard let id = record["id"] as? Int32 else { continue }
            
            
            
            
            let entity = Post(context: context)
            entity.id = id
            entity.title = record["title"] as? String
            entity.created = record["created"] as? Double ?? Date().timeIntervalSince1970
            entity.lastUpdate = record["updated"] as? Double  ?? Date().timeIntervalSince1970
            entity.order = record["_order"] as? Int32  ?? 0
            entity.thumb = record["thumb"] as? String
            entity.cover = record["cover"] as? String
            
            if let content = record["content_data"] as? [[String: Any]] {
                do {
                    let data = try JSONSerialization.data(withJSONObject: content, options: [])
                    entity.content = String.init(data: data, encoding: .utf8)
                }
                catch let error as NSError {
                    print("⚠️ ", String(describing: type(of: self)),":", #function, " cannot parse ", error.debugDescription)
                    entity.content = nil
                }
            } else {
               entity.content = nil
            }
            //            entity.content = record["content_data"] as? String
        }
    }
    
    var thumbUrl: URL? {
        guard let str = thumb else { return nil }
        return URL(string: str)
    }
    
    var coverUrl: URL? {
        guard let str = cover else { return nil }
        return URL(string: str)
    }
    
    var contentItems: [[String: Any]] {
        if let str = self.content, let data = str.data(using: .utf8) {
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                return json as? [[String: Any]] ?? []
                
            } catch let error as NSError {
                print("⚠️ ", String(describing: type(of: self)),":", #function, " ", error.debugDescription)
            }
        }
        
        return []
    }
}
