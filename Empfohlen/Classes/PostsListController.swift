//
//  PostsListController.swift
//  Empfohlen
//
//  Created by Aleksandr Zhovtyi on 11/29/17.
//Copyright © 2017 Aleksandr Zhovtyi. All rights reserved.
//

import UIKit
import CoreData
import SVProgressHUD


//    MARK: - Properties & variables
final class PostsListController: UIViewController {
    //    MARK: Public
    
    //    MARK: Outlets
	@IBOutlet weak var tableView: UITableView!
    
    var managedObjectContext: NSManagedObjectContext = Database.default.viewContext
    
    //    MARK: Private
    fileprivate var _fetchedResultsController: NSFetchedResultsController<Post>? = nil
    fileprivate var requestToken: RequestToken?
    fileprivate var loader: PostsLoader!
    
    var imageProviders: [IndexPath: ImageProvider] = [:]
    var imagePrefetchProviders = Set<ImageProvider>()
    var refreshControl : UIRefreshControl!
    
    //    MARK: Private
    
    @IBAction func didTapTest(_ sender: Any) {
    }
    //    MARK: Enums & Structures
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.loader = PostsLoader()
    }
}

//    MARK: - View life cycle
extension PostsListController  {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Customize appearance
        Appearance.customize(viewController: self)
        UIApplication.shared.statusBarStyle = .lightContent
        navigationItem.backBarButtonItem?.title = " "
        
        setup(tableView)

        DispatchQueue.global(qos: .background).async {
            [weak self] in
            self?.reloadDataFromServer()
        }
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(reloadDataFromServer), for: .valueChanged)
        tableView.addSubview(refreshControl)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let indexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
}

//    MARK: - Utilities
extension PostsListController  {
    fileprivate func setup(_ viewToSetup: UIView) {
        switch viewToSetup {
        case tableView: self.view.layoutSubviews()
//            tableView.register(PostListTableCell.self)
        default: break
        }
        
    }
    
    // MARK: Server
    @IBAction fileprivate func reloadDataFromServer() {
        let fetchRequet: NSFetchRequest<Post> = Post.fetchRequest()
        let count: Int = try! self.managedObjectContext.count(for: fetchRequet)
        if count == 0 {
            SVProgressHUD.setDefaultMaskType(.black)
            SVProgressHUD.show()
        }
        
        
        self.requestToken = self.loader.load(completion: {
            [weak self] error in
            
            defer {
                self?.requestToken = nil
                self?.refreshControl.endRefreshing()
            }
            
            guard self != nil else { return }
            guard error == nil else {
                
                let completion = { Alert.default.showError(message: error!.localizedDescription) }
                
                if SVProgressHUD.isVisible() {
                    SVProgressHUD.dismiss(completion: completion)
                } else {
                    completion()
                }
                
                return
            }
            
            if SVProgressHUD.isVisible() {
                SVProgressHUD.dismiss()
            }
            
        })
    }
}

extension PostsListController {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

//    MARK: - Outlet functions
extension PostsListController  {
    //    MARK: Buttons
    //    MARK: Gesture handlers
}

// MARK: - Navigation & SegueHandler protocol
extension PostsListController: SegueHandler {
    enum SegueType: String {
        case presentMenu
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        switch segueType(for: identifier) {
        case .presentMenu: return true
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segueType(for: segue) {
        case .presentMenu: break
        }
    }
}


// MARK: - Table View
extension PostsListController: UITableViewDataSource, UITableViewDelegate {
    // MARK: Datasource
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.fetchedResultsController.sections?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionInfo = self.fetchedResultsController.sections![section]
        return sectionInfo.numberOfObjects
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return tableView.dequeueReusableCell(PostListTableCell.self, for: indexPath)
    }
    
    // MARK: Displaying
    func configure(_ cell: UITableViewCell, with post: Post) {
        guard let cell = cell as? PostListTableCell else { return }
        
        
        cell.body = post.title
        
        // Cover image
        if let url = post.thumbUrl {
            if let image = ImageCache.default.image(forKey: url) {
                cell.postImage = image
            } else {
                let imageProvider = ImageProvider(imageURL: url) {
                    [weak cell] image in
                    OperationQueue.main.addOperation {
                        cell?.postImage = image
                    }
                }
                
                if let indexPath = self.fetchedResultsController.indexPath(forObject: post) {
                    self.imageProviders[indexPath] = imageProvider
                }
            }
            
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        configure(cell, with: fetchedResultsController.object(at: indexPath))
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
//        print("👍 ", String(describing: type(of: self)),":", #function, " ", indexPath)
        
        
        guard let provider = imageProviders[indexPath] else { return }
        provider.cancel()
        imageProviders[indexPath] = nil
        
//        let post = fetchedResultsController.object(at: indexPath)
//        for provider in imageProviders.filter({ $0.imageURL == post.thumbUrl }){
//            provider.cancel()
//            imageProviders.remove(provider)
//        }
    }
    
    // MARK: Selection
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let post = fetchedResultsController.object(at: indexPath)
        let vc = BrowserController(with: post)
        navigationController?.pushViewController(vc, animated: true)
    }
}

// MARK: - Fetched results controller
extension PostsListController: NSFetchedResultsControllerDelegate {
    var fetchedResultsController: NSFetchedResultsController<Post> {
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let fetchRequest: NSFetchRequest<Post> = Post.fetchRequest()
        fetchRequest.fetchBatchSize = 10
        
        fetchRequest.sortDescriptors = [
            NSSortDescriptor(key: #keyPath(Post.lastUpdate), ascending: false),
            NSSortDescriptor(key: #keyPath(Post.order), ascending: true)
        ]
        
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        aFetchedResultsController.delegate = self
        _fetchedResultsController = aFetchedResultsController
        
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            let nserror = error as NSError
            fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
        }
        
        return _fetchedResultsController!
    }
    
    
    
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        switch type {
        case .insert:
            tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        default:
            return
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .fade)
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .fade)
        case .update:
            if let indexPath = indexPath, let cell = tableView.cellForRow(at: indexPath), let post = anObject as? Post {
                configure(cell, with: post)
            }
            
        case .move:
            if let indexPath = indexPath, let cell = tableView.cellForRow(at: indexPath), let post = anObject as? Post {
                configure(cell, with: post)
            }
            
            tableView.moveRow(at: indexPath!, to: newIndexPath!)
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
}

// MARK: - Lauch animation
extension PostsListController: LaunchAnimationDestinationController {
    var destinationImageView: UIImageView {
        guard let titleView = navigationItem.titleView as? NavigationTitleView else {
            assertionFailure("⚠️ incorrect title view")
            return UIImageView()
        }
        return titleView.imageView
    }
}
