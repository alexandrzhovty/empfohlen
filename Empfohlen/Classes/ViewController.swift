//
//  ViewController.swift
//  Empfohlen
//
//  Created by Aleksandr Zhovtyi on 12/13/17.
//  Copyright © 2017 Aleksandr Zhovtyi. All rights reserved.
//

import UIKit

extension UIViewController {
    open func present(_ viewControllerToPresent: UIViewController, animated flag: Bool) {
        self.present(viewControllerToPresent, animated: flag, completion: nil)
    }
    
    open func dismiss(animated flag: Bool) {
        self.dismiss(animated: flag, completion: nil)
    }
}
