//
//  PostListNotificationView.swift
//  Empfohlen
//
//  Created by Aleksandr Zhovtyi on 12/1/17.
//  Copyright © 2017 Aleksandr Zhovtyi. All rights reserved.
//

import UIKit

import SafariServices

class PostListNotificationView: UIView {

    @IBOutlet weak fileprivate var titleLabel: UILabel!
    @IBOutlet weak fileprivate var bodyLabel: ActiveLabel!
    @IBOutlet weak fileprivate var button: UIButton!
    
    deinit {
        unregisterObserver()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let spacing: CGFloat = 8.0
        button.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, spacing);
        button.titleEdgeInsets = UIEdgeInsetsMake(0, spacing, 0, 0);
        
        bodyLabel.enabledTypes = [.url]
        bodyLabel.handleURLTap {
            [weak self] url in
            guard let `self` = self else { return }
            
            let vc = self.viewController()!
            let svc = SFSafariViewController(url: url)
            vc.present(svc, animated: true, completion: nil)
        }
        
        
        subviews.forEach{ configure($0) }
        
        registerObserver()
    }
    
    @IBAction fileprivate func didTapButton(_ sender: UIButton) {
        let url = URL(string: "https://www.empfohlen.de/anmelden/")!
        let vc = self.viewController()!
        let svc = SFSafariViewController(url: url)
        vc.present(svc, animated: true, completion: nil)
    }

}

// MARK: - Utiities
extension PostListNotificationView {
    private func configure(_ viewToConfig: UIView) {
        switch viewToConfig {
        case bodyLabel:
            if let notification = AppDelegate.shared.lastRecievedRemoteNotiifcation {
                bodyLabel.text = notification.request.content.body
            } else {
                bodyLabel.text = NSLocalizedString("Keine neuen Nachrichten.", comment: "")
            }
            
        default: break
        }
    }
}

// MARK: - Observer
extension PostListNotificationView: ObserverProtocol {
    func registerObserver() {
        let center = NotificationCenter.default
        let main = OperationQueue.main
        
        observers.append(
            center.addObserver(forName: .didReceiveRemoteNotification, object: nil, queue: main, using: {
                [weak self] notification in
                guard let `self` = self else { return }
                self.configure(self.bodyLabel)
            })
        )
    }
}
