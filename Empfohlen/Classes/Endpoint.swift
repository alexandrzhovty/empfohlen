//
//  EndPoint.swift
//  Empfohlen
//
//  Created by Aleksandr Zhovtyi on 12/4/17.
//  Copyright © 2017 Aleksandr Zhovtyi. All rights reserved.
//

import Foundation

enum Endpoint {
    case preferences
    case menu(page: Int)
    case post(page: Int)
    case token(String)
}

extension Endpoint {
    static var urlComponents: URLComponents {
        var components = URLComponents()
        components.scheme = "http"
        components.host = "empfohlendeapp.devinstance.de"
        return components
    }
    
    var request: URLRequest {
        
        var components = Endpoint.urlComponents
        
        
        
        var request: URLRequest
        
        switch self {
        case .preferences:
            components.path = "/lvjsonapi2getoptions"
            request = URLRequest(url: components.url!)
            
        case .menu(let page):
            components.path = "/lvjsonapi2getposts"
            components.queryItems = [
                URLQueryItem(name: "type", value: "page"),
                URLQueryItem(name: "orderby", value: "date"),
                URLQueryItem(name: "order", value: "desc"),
                URLQueryItem(name: "page", value: "\(page)")
            ]
            request = URLRequest(url: components.url!)
            
        case .post(let page):
            components.path = "/lvjsonapi2getposts"
            components.queryItems = [
                URLQueryItem(name: "type", value: "post"),
                URLQueryItem(name: "orderby", value: "date"),
                URLQueryItem(name: "order", value: "desc"),
                URLQueryItem(name: "page", value: "\(page)")
            ]
            request = URLRequest(url: components.url!)
        
        case .token(let value):
            components.path = "/thepushserver/savetoken/"
            components.queryItems = [
                URLQueryItem(name: "device_token", value: value),
                URLQueryItem(name: "device_type", value: "ios")
            ]
            
             request = URLRequest(url: components.url!)
        }
        
        
        
        return request
    }
}
