//
//  Image.swift
//  Empfohlen
//
//  Created by Aleksandr Zhovtyi on 11/29/17.
//Copyright © 2017 Aleksandr Zhovtyi. All rights reserved.
//

import UIKit

//    MARK: - Asset image types
// Iteratable file should be added to the project
protocol ImageAssetType: Testable  {}

typealias Image = UIImage

extension UIImage {
    struct Logo {
        private init(){}
        static let white = UIImage(named: "Logo-White")!
        static let navigation = UIImage(named: "Logo-Nav")!
    }
}



//    MARK: - Initialization
extension UIImage {
    convenience init<T>(_ imageAssetType: T) where T: ImageAssetType, T: RawRepresentable, T.RawValue == String  {
        self.init(named: imageAssetType.rawValue)!
    }
}
extension UIImage {
    convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
    
}

extension UIImage {
    
    enum GradientDirection {
        case horizontal
    }
    
    struct GradientPoint {
        var location: CGFloat
        var color: UIColor
    }
    
    convenience init?(size: CGSize, startColor: UIColor, endColor: UIColor, gradientDirection: GradientDirection) {
        UIGraphicsBeginImageContextWithOptions(size, false, UIScreen.main.scale)
        
        
        
        guard let context = UIGraphicsGetCurrentContext() else { return nil }       // If the size is zero, the context will be nil.
        guard let gradient = CGGradient(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: [startColor.cgColor, endColor.cgColor] as CFArray, locations: [0, 0.4])
            else { return nil }
        
        let position: (start: CGPoint, end: CGPoint)
        
        switch gradientDirection {
        case .horizontal: position = (CGPoint.zero, CGPoint(x: size.width, y: 0))
        }
        
        context.drawLinearGradient(gradient, start: position.start, end: position.end, options: CGGradientDrawingOptions())
        guard let image = UIGraphicsGetImageFromCurrentImageContext()?.cgImage else { return nil }
        self.init(cgImage: image)
        defer { UIGraphicsEndImageContext() }
        
    }
    
    
}


