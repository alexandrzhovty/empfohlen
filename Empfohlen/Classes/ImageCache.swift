//
//  ImageCache.swift
//  Famous
//
//  Created by Aleksandr Zhovtyi on 10/26/17.
//  Copyright © 2017 Empfohlen. All rights reserved.
//

import UIKit

struct ImageCache {
    static let `default` = ImageCache()
    
    typealias DownloadIdentifier = StringIdentifier
    
    
    static var budleID: String {
        return  "\(Bundle.main.bundleIdentifier!).\(String(describing: ImageCache.self))"
    }
    
    fileprivate let concurrentPhotoQueue = DispatchQueue(label: ImageCache.budleID, attributes: .concurrent)
    fileprivate var memoryCache: NSCache<NSString, UIImage>
    
    private init() {
        memoryCache = NSCache<NSString, UIImage>()
        memoryCache.countLimit = 10
    }
    
    func clearCache() {
        memoryCache.removeAllObjects()
        
        
        
        let url = FileManager.cachedImagesDirectory
        
        print(String(describing: type(of: self)),":", #function, " ", url.path)
        
        do {
            let fileNames = try FileManager.default.contentsOfDirectory(atPath: url.path)
            for fileName in fileNames {
                let filePathName = "\(url.path)/\(fileName)"
                try? FileManager.default.removeItem(atPath: filePathName)
            }
        } catch {
            
        }
        
    }
    
}

protocol ImageCacheProtocol {
    func image(forKey url: URL) -> UIImage?
    func set(_ image: UIImage?, forKey url: URL)
}

extension ImageCache: ImageCacheProtocol {
    func image(forKey url: URL) -> UIImage? {
        // Handling readers, writers problem
        
//        return self.memoryCache.object(forKey: url.cacheKey)?.copy() as? UIImage
        
        var imageCopy: UIImage?
        concurrentPhotoQueue.sync {
            imageCopy = self.memoryCache.object(forKey: url.cacheKey)?.copy() as? UIImage
        }
        return imageCopy
        
    }
    
    func set(_ image: UIImage?, forKey url: URL) {
//         handling readers, writers problem
        
        concurrentPhotoQueue.async(flags: .barrier) {
            if let image = image {
                self.memoryCache.setObject(image, forKey: url.cacheKey)
            } else {
                self.memoryCache.removeObject(forKey: url.cacheKey)
            }
        }
    }
}

extension URL {
    var cacheKey: NSString {
        return self.path.replacingOccurrences(of: "/", with: "_") as NSString
    }
}
