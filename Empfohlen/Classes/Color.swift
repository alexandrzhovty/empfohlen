//
//  Color.swift
//  Empfohlen
//
//  Created by Aleksandr Zhovtyi on 11/29/17.
//Copyright © 2017 Aleksandr Zhovtyi. All rights reserved.
//

import UIKit

typealias Color = UIColor

//    MARK: - Color types
enum ColorType: UInt32 {
    case darkBlue = 0x261c46
    case richRed = 0xda1e15
    
    var color: UIColor { return UIColor(self) }
}

//    MARK: - Initialization
extension UIColor {
    convenience init(_ type: ColorType) {
        self.init(rgbValue: type.rawValue)
    }
    
    convenience init(rgbaValue: UInt32) {
        let red   = CGFloat((rgbaValue >> 24) & 0xff) / 255.0
        let green = CGFloat((rgbaValue >> 16) & 0xff) / 255.0
        let blue  = CGFloat((rgbaValue >>  8) & 0xff) / 255.0
        let alpha = CGFloat((rgbaValue      ) & 0xff) / 255.0
        
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
    
    convenience init(rgbValue: UInt32, alpha: CGFloat = 1) {
        let red   = CGFloat((rgbValue >> 16) & 0xff) / 255.0
        let green = CGFloat((rgbValue >>  8) & 0xff) / 255.0
        let blue  = CGFloat((rgbValue      ) & 0xff) / 255.0
        
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
}


//    MARK - Color scheme
extension UIColor {
    
    static var tint: UIColor { return Color(.darkBlue) }
    
    enum Background {
        static var navigation: UIColor { return Color(.darkBlue) }
    }
}
