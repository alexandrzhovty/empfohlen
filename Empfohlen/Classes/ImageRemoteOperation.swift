//
//  ImageLoadOperation.swift
//  Famous
//
//  Created by Aleksandr Zhovtyi on 10/27/17.
//  Copyright © 2017 Empfohlen. All rights reserved.
//

import UIKit

public class ImageRemoteOperation: AsyncOperation {
    fileprivate(set) var url: URL
    fileprivate var outputImage: UIImage?
    

    
    public init(url: URL) {
        self.url = url
        super.init()
    }
    
    override public func main() {

        
        // Check is it necessary to download image
        if let dataProvider = dependencies.filter({ $0 is ImagePass }).first as? ImagePass {
            self.outputImage = dataProvider.image
            if self.outputImage != nil {
                if self.isCancelled { return }
                self.state = .finished
//                self.cancel()
                return
            }
            
        }
        
        let config = URLSessionConfiguration.background(withIdentifier: UUID().uuidString)
        let session = URLSession(configuration: config, delegate: self, delegateQueue: nil)
        
        let task = session.downloadTask(with: url)

        NetworkActivity.started()
        task.resume()
    }
}

// MARK: - Download delegate delegate
extension ImageRemoteOperation: URLSessionDownloadDelegate {
    
    public func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        NetworkActivity.finished()
        session.finishTasksAndInvalidate()
        self.state = .finished
    }
    
    public func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
//        guard self.isCancelled == false else { return }
        
        
        let imageData: Data
        do { imageData = try Data(contentsOf: location) }
        catch { return }
        
//        guard self.isCancelled == false else { return }
        self.outputImage = UIImage(data: imageData)
        
        // Move loaded file to temp dictionary
        let destionationURL = FileManager.cachedImagesDirectory.appendingPathComponent(url.cacheKey as String)
        if FileManager.default.fileExists(atPath: destionationURL.path) == false {
            do { try FileManager.default.moveItem(at: location, to: destionationURL) }
            catch { print("Cannot move file: \(error)") }
            
        }
        
        ImageCache.default.set(self.outputImage, forKey: url)
        
    }
}

// MARK: - Image pass
extension ImageRemoteOperation: ImagePass {
    var image: UIImage? {
        return outputImage
    }
}

