//
//  NavigationDelegate.swift
//  Empfohlen
//
//  Created by Aleksandr Zhovtyi on 11/29/17.
//  Copyright © 2017 Aleksandr Zhovtyi. All rights reserved.
//

import UIKit

class NavigationDelegate: NSObject {
    fileprivate var navigationAnimator: LaunchAnimationController
    
//    required init?(coder aDecoder: NSCoder) {
//        //        fatalError("init(coder:) has not been implemented")
//        navigationAnimator = .init()
//        super.init(coder: aDecoder)
//    }
    
    
    override init() {
        navigationAnimator = .init()
        super.init()
    }
}

extension NavigationDelegate: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationControllerOperation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        switch (fromVC, toVC) {
        case let (from, to) where from is LaunchAnimationSourceController && to is LaunchAnimationDestinationController:
            return LaunchAnimationController()
        default:
            return nil
        }
        
    }
}
