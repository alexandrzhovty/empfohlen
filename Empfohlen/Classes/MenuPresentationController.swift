//
//  MenuPresentationController.swift
//  Empfohlen
//
//  Created by Aleksandr Zhovtyi on 11/30/17.
//  Copyright © 2017 Aleksandr Zhovtyi. All rights reserved.
//

import UIKit

class MenuPresentationController: UIPresentationController {
    
    var dimmingView: UIView! = UIView()
    //    var holderView: UIView?
    var interactor = InteractorTransition()
    let marging: CGFloat = 8.0
    var panGestures: Set<UIPanGestureRecognizer>

    
    deinit {
//        print("🚀 ", String(describing: type(of: self)),":", #function)
    }
    
    override init(presentedViewController: UIViewController, presenting presentingViewController: UIViewController?) {
        panGestures = .init()
        
        super.init(presentedViewController: presentedViewController, presenting: presentingViewController)
        
        dimmingView.alpha = 0.0
        
        
        
        
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(_:)))
        presentedViewController.view.addGestureRecognizer(panGesture)
        panGestures.insert(panGesture)
        
        let panGesture1 = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(_:)))
        dimmingView.addGestureRecognizer(panGesture1)
        panGestures.insert(panGesture1)
        
        
        dimmingView.addGestureRecognizer(
            UITapGestureRecognizer(target: self, action: #selector(handleTapGesture(_:)))
        )
       
        
    }
    
    override var frameOfPresentedViewInContainerView : CGRect {
        var presentedViewFrame = CGRect.zero
        let containerBounds = containerView!.bounds
        presentedViewFrame.size = size(forChildContentContainer: presentedViewController, withParentContainerSize: containerBounds.size)
        presentedViewFrame.origin.x = containerBounds.width - presentedViewFrame.width
        return presentedViewFrame
    }
    
    override func size(forChildContentContainer container: UIContentContainer, withParentContainerSize parentSize: CGSize) -> CGSize {
        var size = parentSize
        size.width = size.width * 4 / 5
        return size
    }
    
    override func presentationTransitionWillBegin() {
        
        // Setup diminig view
        dimmingView.frame = self.containerView?.bounds ?? UIScreen.main.bounds
        dimmingView.alpha = 0.0
        dimmingView.backgroundColor = .clear
        dimmingView.isOpaque = false
//        containerView?.insertSubview(dimmingView, at:0)
        containerView?.addSubview(dimmingView)
        
        
        
        
        let coordinator = presentedViewController.transitionCoordinator
        if (coordinator != nil) {
            coordinator!.animate(alongsideTransition: {
                (context:UIViewControllerTransitionCoordinatorContext!) -> Void in
                self.dimmingView.alpha = 1.0
            }, completion:nil)
        } else {
            dimmingView.alpha = 1.0
        }
    }
    
    override func presentationTransitionDidEnd(_ completed: Bool) {
        // The value of the 'completed' argument is the same value passed to the
        // -completeTransition: method by the animator.  It may
        // be NO in the case of a cancelled interactive transition.
        if completed == false {
            // The system removes the presented view controller's view from its
            // superview and disposes of the containerView.  This implicitly
            // removes the views created in -presentationTransitionWillBegin: from
            // the view hierarchy.  However, we still need to relinquish our strong
            // references to those view.
            self.dimmingView = nil
            //            self.holderView?.removeFromSuperview()
            //            self.holderView = nil
        }
    }
    
    override func dismissalTransitionWillBegin() {
        let coordinator = presentedViewController.transitionCoordinator
        if (coordinator != nil) {
            coordinator!.animate(alongsideTransition: { (context:UIViewControllerTransitionCoordinatorContext!) -> Void in
                self.dimmingView.alpha = 0.0
            }, completion:nil)
        } else {
            dimmingView.alpha = 0.0
        }
    }
    
    override func dismissalTransitionDidEnd(_ completed: Bool) {
        // The value of the 'completed' argument is the same value passed to the
        // -completeTransition: method by the animator.  It may
        // be NO in the case of a cancelled interactive transition.
        if completed == true {
            // The system removes the presented view controller's view from its
            // superview and disposes of the containerView.  This implicitly
            // removes the views created in -presentationTransitionWillBegin: from
            // the view hierarchy.  However, we still need to relinquish our strong
            // references to those view.
            self.dimmingView = nil
            //            self.holderView?.removeFromSuperview()
            //            self.holderView = nil
        }
    }
    
    
    
    override func containerViewWillLayoutSubviews() {
        dimmingView.frame = (containerView?.bounds)!
        presentedView!.frame = frameOfPresentedViewInContainerView
    }
    
    
    
}


// MARK:  - Recognizer handlers
extension MenuPresentationController: UIGestureRecognizerDelegate {
    @objc func handleTapGesture(_ recognizer: UITapGestureRecognizer) {
        self.presentingViewController.dismiss(animated: true, completion: nil)
    }
    
    @objc func handlePanGesture(_ recognizer: UIPanGestureRecognizer) {
        
        guard let view = recognizer.view else { return }
        
        let percentThreshold:CGFloat = 0.3
        let translation = recognizer.translation(in: view)
        
        let progress: CGFloat
        if translation.x < 0 {
            progress = 0
        } else {
            let horizontalMovement = abs(translation.x) / self.presentedViewController.view.bounds.width
            let downwardMovement = fmaxf(Float(horizontalMovement), 0.0)
            let downwardMovementPercent = fminf(downwardMovement, 1.0)
            progress = CGFloat(downwardMovementPercent)
        }
        
        
        switch recognizer.state {
        case .began:
            interactor.hasStarted = true
            self.presentingViewController.dismiss(animated: true, completion: nil)
        case .changed:
            interactor.shouldFinish = progress > percentThreshold
            interactor.update(progress)
        case .cancelled:
            interactor.hasStarted = false
            interactor.cancel()
        case .ended:
            interactor.hasStarted = false
            interactor.shouldFinish ? interactor.finish() : interactor.cancel()
        default:
            break
        }
        
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        guard
            let panGesture = gestureRecognizer as? UIPanGestureRecognizer,
            panGestures.contains(panGesture)
        else { return false }

        // Horisontal movement
        let translation = panGesture.translation(in: panGesture.view?.superview)
        return  fabs(translation.x) > fabs(translation.y)
    }
}
