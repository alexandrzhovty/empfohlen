//
//  URL.swift
//  Empfohlen
//
//  Created by Aleksandr Zhovtyi on 12/1/17.
//  Copyright © 2017 Aleksandr Zhovtyi. All rights reserved.
//


import Foundation

///
/// Tired of using URL(string: "url")! for static URLs in #swiftlang? Make URL conform to
/// ExpressibleByStringLiteral and you can now use "url" 🎉
///
extension URL: ExpressibleByStringLiteral {
    // By using 'StaticString' we disable string interpolation, for safety
    public init(stringLiteral value: StaticString) {
        self = URL(string: "\(value)")!
    }
}

