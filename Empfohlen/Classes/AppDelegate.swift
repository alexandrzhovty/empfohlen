//
//  AppDelegate.swift
//  Empfohlen
//
//  Created by Aleksandr Zhovtyi on 11/29/17.
//  Copyright © 2017 Aleksandr Zhovtyi. All rights reserved.
//

import UIKit
import CoreData
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder {

    var window: UIWindow?
    
    static var shared: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    var rootNavigationController: UINavigationController? {
        guard let navVC = self.window?.rootViewController as? UINavigationController else {
            return nil
        }
        return navVC
    }
    
    private var requestToken: RequestToken?
    var lastRecievedRemoteNotiifcation: UNNotification?
    

}

// MARK: - Application delegates
extension AppDelegate: UIApplicationDelegate {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        Preferences.default.registerDefaults()
        Appearance.customize()
        FileManager.createDirectoryForCachedImages()
        registerForPushNotifications(application)
        UIApplication.shared.applicationIconBadgeNumber = 0
        return true
    }
    
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        UIApplication.shared.applicationIconBadgeNumber = 0
    }

}

// MARK: - Notification
extension AppDelegate: UNUserNotificationCenterDelegate {
    func registerForPushNotifications(_ application: UIApplication) {
        UNUserNotificationCenter.current().delegate = self
        
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: {_, _ in })
        application.registerForRemoteNotifications()
        
    }
    
//    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
//
//    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        let request = Endpoint.token(token).request
        let task = URLSession.shared.dataTask(with: request) {
            [weak self] (_, _, _) in
            self?.requestToken = nil
        }
        task.resume()
        self.requestToken = RequestToken(task: task)
        
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        self.lastRecievedRemoteNotiifcation = notification
        NotificationCenter.default.post(name: .didReceiveRemoteNotification, object: notification)
        
        completionHandler([ .sound, .alert ])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        self.lastRecievedRemoteNotiifcation = response.notification
        NotificationCenter.default.post(name: .didReceiveRemoteNotification, object: response.notification)
        completionHandler()
    }
}

