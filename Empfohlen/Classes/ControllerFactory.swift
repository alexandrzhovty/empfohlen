//
//  ControllerFactory.swift
//  Empfohlen
//
//  Created by Aleksandr Zhovtyi on 12/13/17.
//  Copyright © 2017 Aleksandr Zhovtyi. All rights reserved.
//

import Foundation
import CoreData

protocol StoryboardControllerFactory {
    func createExpandedListController(with menu: Menu, using managedObjectContext: NSManagedObjectContext) -> ExpandedListController
}

class ControllerFactory {
    
}

extension ControllerFactory: StoryboardControllerFactory {
    func createExpandedListController(with menu: Menu, using managedObjectContext: NSManagedObjectContext) -> ExpandedListController {
        let vc = Storyboard.list.instantiateViewController(ExpandedListController.self)
        vc.managedObjectContext = managedObjectContext
        vc.menu = menu
        return vc
    }
}
