//
//  MenuController.swift
//  Empfohlen
//
//  Created by Aleksandr Zhovtyi on 11/30/17.
//Copyright © 2017 Aleksandr Zhovtyi. All rights reserved.
//

import UIKit
import CoreData
import SVProgressHUD
import StoreKit

//    MARK: - Properties & variables
final class MenuController: UIViewController {
    //    MARK: Public
    
    //    MARK: Outlets
	@IBOutlet weak var tableView: UITableView!
    var managedObjectContext: NSManagedObjectContext = Database.default.viewContext
    
    //    MARK: Private
    fileprivate weak var presenter: MenuPresentationController?
    fileprivate var _fetchedResultsController: NSFetchedResultsController<Menu>? = nil
    
    fileprivate var imageProvider: ImageProvider?
    fileprivate var requestToken: RequestToken?
    fileprivate var menuLoader: MenuLoader!
    
    // MARK: Initialization
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.modalPresentationStyle = .custom
        self.transitioningDelegate = self
        self.menuLoader = MenuLoader()
    }
    
    deinit {
//        print("🚀 ", String(describing: type(of: self)),":", #function)
    }
}

//    MARK: - View life cycle
extension MenuController  {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Customize appearance
        Appearance.customize(viewController: self)
        
        
        
        DispatchQueue.global(qos: .background).async {
            [weak self] in
            guard let `self` = self else { return }
            
            let fetchRequet: NSFetchRequest<Menu> = Menu.fetchRequest()
            let count: Int = try! self.managedObjectContext.count(for: fetchRequet)
            if count == 0 {
                SVProgressHUD.setDefaultMaskType(.black)
                SVProgressHUD.show()
            }
            
            
            self.requestToken = self.menuLoader.load(completion: {
                [weak self] error in
                
                defer { self?.requestToken = nil }
                
                guard self != nil else { return }
                guard error == nil else {
                    
                    let completion = { Alert.default.showError(message: error!.localizedDescription) }
                    
                    if SVProgressHUD.isVisible() {
                        SVProgressHUD.dismiss(completion: completion)
                    } else {
                        completion()
                    }
                    
                    return
                }
                
                if SVProgressHUD.isVisible() {
                    SVProgressHUD.dismiss()
                }
                
            })
        }
        
    }
}

////    MARK: - Utilities
//extension MenuController  {
//    fileprivate func setup(_ viewToSetup: UIView) {
//        switch viewToSetup {
//        case tableView:
//            tableView.register(MenuTableCell.self) default: break
//        }
//        
//    }
//}

//    MARK: - Outlet functions
extension MenuController  {
    //    MARK: Buttons
    //    MARK: Gesture handlers
}

// MARK: - Navigation & SegueHandler protocol
extension MenuController: SegueHandler {
    enum SegueType: String {
        case none
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        switch segueType(for: identifier) {
        case .none: return true
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segueType(for: segue) {
        case .none: break
        }
    }
}

// MARK: - Table view
extension MenuController: UITableViewDataSource, UITableViewDelegate {
    // MARK: Datasource
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.fetchedResultsController.sections?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionInfo = self.fetchedResultsController.sections![section]
        return sectionInfo.numberOfObjects
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return tableView.dequeueReusableCell(MenuTableCell.self, for: indexPath)
    }
    
    // MARK: Displaying
    func configure(_ cell: UITableViewCell, with menu: Menu) {
        guard let cell = cell as? MenuTableCell else { return }
        
        cell.title = menu.title
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        configure(cell, with: fetchedResultsController.object(at: indexPath))
    }
    
    
    // MARK: Selection
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let menu = fetchedResultsController.object(at: indexPath)
        
        let navVC = AppDelegate.shared.rootNavigationController
        
        switch menu.displayMode {
        case .home:
            navVC?.popToRootViewController(animated: false)
       
            
        case .default:
            let vc = BrowserController(with: menu)
            let first = navVC!.viewControllers.first!
            navVC?.setViewControllers([first, vc], animated: false)
            
        case .list:
            let vc = ControllerFactory().createExpandedListController(with: menu, using: managedObjectContext)
            let first = navVC!.viewControllers.first!
            navVC?.setViewControllers([first, vc], animated: false)
        
        case .itunes:
            
            if #available(iOS 10.3, *) {
                SKStoreReviewController.requestReview()
            } else {
                // Fallback on earlier versions
//                print("👍 ", String(describing: type(of: self)),":", #function, " itunes \n", menu.content ?? "nil")
                
                let appID = Constants.iTunesAppID
                //            let urlStr = "itms-apps://itunes.apple.com/app/id\(appID)" // (Option 1) Open App Page
                //            let urlStr = "itms-apps://itunes.apple.com/app/viewContentsUserReviews?id=\(appID)&pageNumber=0&sortOrdering=2&type=Purple+Software&mt=8" // (Option 2) Open App Review Tab
                let urlStr = "http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=\(appID)&pageNumber=0&sortOrdering=2&type=Purple+Software&mt=8"
                let url = URL(string: urlStr)!
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
            
            tableView.deselectRow(at: indexPath, animated: true)
            
            return
           

        case .link:
            guard let input = menu.content else { return }
            
            let detector: NSDataDetector
            do { detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue) }
            catch { return }
            
            let matches = detector.matches(in: input, options: [], range: NSRange(location: 0, length: input.utf16.count))
            
            for match in matches {
                guard let range = Range(match.range, in: input) else { continue }
                let sUrl = input[range]
                if let url = URL(string: String(sUrl)) {
                    print(url)
                    let vc = BrowserController(url: url)
                    let first = navVC!.viewControllers.first!
                    navVC?.setViewControllers([first, vc], animated: false)
                    break
                }
            }
        }
        
        dismiss(animated: true)
        
        
    }
}

// MARK: - Fetched results controller
extension MenuController: NSFetchedResultsControllerDelegate {
    var fetchedResultsController: NSFetchedResultsController<Menu> {
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let fetchRequest: NSFetchRequest<Menu> = Menu.fetchRequest()
        fetchRequest.fetchBatchSize = 10
        
        fetchRequest.sortDescriptors = [
            NSSortDescriptor(key: #keyPath(Menu.order), ascending: true),
            NSSortDescriptor(key: #keyPath(Menu.created), ascending: true)
        ]
        
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        aFetchedResultsController.delegate = self
        _fetchedResultsController = aFetchedResultsController
        
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            let nserror = error as NSError
            fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
        }
        
        return _fetchedResultsController!
    }
    
    
    
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        switch type {
        case .insert:
            tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        default:
            return
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .fade)
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .fade)
        case .update:
            if let indexPath = indexPath, let cell = tableView.cellForRow(at: indexPath), let menu = anObject as? Menu {
                configure(cell, with: menu)
            }
            
        case .move:
            if let indexPath = indexPath, let cell = tableView.cellForRow(at: indexPath), let menu = anObject as? Menu {
                configure(cell, with: menu)
            }
            
            tableView.moveRow(at: indexPath!, to: newIndexPath!)
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
}


// MARK: - Transitioning
extension MenuController: UIViewControllerTransitioningDelegate {
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        let presenter = MenuPresentationController(presentedViewController: presented, presenting: presenting)
        self.presenter = presenter
        return self.presenter
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let animator = MenuPresentationAnimator()
        animator.isPresentation = true
        return animator
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let animator = MenuPresentationAnimator()
        animator.isPresentation = false
        return animator
    }
    
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        guard let pressenter = self.presenter else { return nil }
        return pressenter.interactor.hasStarted ? pressenter.interactor : nil
    }
    
}
