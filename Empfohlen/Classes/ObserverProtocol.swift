//
//  ObserverProtocol.swift
//  Empfohlen
//
//  Created by Aleksandr Zhovtyi on 11/30/17.
//Copyright © 2017 Aleksandr Zhovtyi. All rights reserved.
//

import UIKit
import ObjectiveC.runtime

protocol ObserverProtocol: class {
    var observers: [Any] { get set }
    func registerObserver()
    func unregisterObserver()
}

private let __observersKey = "ObserverProtocol_observers"
extension ObserverProtocol where Self: Any{
    var observers: [Any] {
        get {
            if let array =  objc_getAssociatedObject(self, __observersKey) as? [Any] {
                return array
            }
            let array = [Any]()
            objc_setAssociatedObject(self, __observersKey, array, .OBJC_ASSOCIATION_COPY)
            return array
        }
        
        set {
            objc_setAssociatedObject(self, __observersKey, newValue, .OBJC_ASSOCIATION_COPY)
        }
    }
    
    func unregisterObserver() {
        observers.forEach { NotificationCenter.default.removeObserver($0) }
        observers.removeAll()
        objc_setAssociatedObject(self, __observersKey, nil, .OBJC_ASSOCIATION_COPY)
        
    }
}
