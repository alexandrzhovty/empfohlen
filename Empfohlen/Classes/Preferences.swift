//
//  Settings.swift
//  Empfohlen
//
//  Created by Aleksandr Zhovtyi on 12/4/17.
//  Copyright © 2017 Aleksandr Zhovtyi. All rights reserved.
//

import Foundation


final class Preferences {
    static var `default`: Preferences = .init()
    
    private(set) var userDefaults: UserDefaults
    var testProperties: Any?
    
    init(using userDefaults: UserDefaults = .standard) {
        self.userDefaults = userDefaults
    }
    
    func registerDefaults() {
        let params: [String : Any] = [:]
        userDefaults.register(defaults: params)
    }
    
    func restoreDefaults()  {
        
    }
    
    
    @objc var appLogo: URL? {
        get { return userDefaults.url(forKey: #function) }
        set { userDefaults.set(newValue, forKey: #function) }
    }
    
    @objc var headerText: String? {
        get { return userDefaults.string(forKey: #function) }
        set { userDefaults.set(newValue, forKey: #function) }
    }
    
    @objc var contentButtonTitle: String? {
        get { return userDefaults.string(forKey: #function) }
        set { userDefaults.set(newValue, forKey: #function) }
    }
    
    var contentButtonUrl: URL? {
        get { return userDefaults.url(forKey: #function) }
        set { userDefaults.set(newValue, forKey: #function) }
    }
    
    var contentLogoUrl: URL? {
        get { return userDefaults.url(forKey: #function) }
        set { userDefaults.set(newValue, forKey: #function) }
    }
    
    @objc var contentText: String? {
        get { return userDefaults.string(forKey: #function) }
        set { userDefaults.set(newValue, forKey: #function) }
    }
    
    @objc var footerButtonTitle: String? {
        get { return userDefaults.string(forKey: #function) }
        set { userDefaults.set(newValue, forKey: #function) }
    }
    
    @objc var footerLogoUrl: URL? {
        get { return userDefaults.url(forKey: #function) }
        set { userDefaults.set(newValue, forKey: #function) }
    }
    
    @objc var footerButtonUrl: URL? {
        get { return userDefaults.url(forKey: #function) }
        set { userDefaults.set(newValue, forKey: #function) }
    }
    
    @objc var footerSubtitle: String? {
        get { return userDefaults.string(forKey: #function) }
        set { userDefaults.set(newValue, forKey: #function) }
    }
    
    @objc var footerTitle: String? {
        get { return userDefaults.string(forKey: #function) }
        set { userDefaults.set(newValue, forKey: #function) }
    }
    
    
   
    
    @objc var rightSidebarButtonTitle: String? {
        get { return userDefaults.string(forKey: #function) }
        set { userDefaults.set(newValue, forKey: #function) }
    }
    
    @objc var rightSidebarButtonURL: URL? {
        get { return userDefaults.url(forKey: #function) }
        set { userDefaults.set(newValue, forKey: #function) }
    }
    
    @objc var rightSidebarLogo: URL? {
        get { return userDefaults.url(forKey: #function) }
        set { userDefaults.set(newValue, forKey: #function) }
    }
    
    @objc var rightSidebarTitle: String? {
        get { return userDefaults.string(forKey: #function) }
        set { userDefaults.set(newValue, forKey: #function) }
    }
    
    @objc var rightSidebarSubtitle: String? {
        get { return userDefaults.string(forKey: #function) }
        set { userDefaults.set(newValue, forKey: #function) }
    }

    
    func parse(_ json: [String: Any]) {
        
        self.rightSidebarLogo = Preferences.urlFromString(obj: json["right_sidebar_logo"])
        self.rightSidebarTitle = json["right_sidebar_title"] as? String
        self.rightSidebarSubtitle = json["right_sidebar_subtitle"] as? String
        
        self.rightSidebarButtonTitle = json["right_sidebar_button_title"] as? String
        self.rightSidebarButtonURL = Preferences.urlFromString(obj: json["right_sidebar_button_url"])
        
        self.appLogo = Preferences.urlFromString(obj: json["app_logo"])
        
        headerText = json["header_text"] as? String
        contentButtonTitle = json["content_button_title"] as? String
        
        self.contentButtonUrl = Preferences.urlFromString(obj: json["content_button_url"])
        self.contentLogoUrl = Preferences.urlFromString(obj: json["content_logo"])
        self.contentText = json["content_text"] as? String
        
        self.footerButtonTitle = json["footer_button_title"] as? String
        self.footerButtonUrl = Preferences.urlFromString(obj: json["footer_button_url"])
        
        self.footerLogoUrl = Preferences.urlFromString(obj: json["footer_logo"])
        
        self.footerTitle = json["footer_title"] as? String
        self.footerSubtitle = json["footer_subtitle"] as? String
        
        
    }
    
    static func urlFromString(obj: Any?) -> URL? {
        guard let str = obj as? String else { return nil }
        return URL(string: str)
    }
}
