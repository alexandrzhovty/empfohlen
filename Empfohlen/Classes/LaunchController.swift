//
//  LaunchScreenController.swift
//  Empfohlen
//
//  Created by Aleksandr Zhovtyi on 11/29/17.
//Copyright © 2017 Aleksandr Zhovtyi. All rights reserved.
//

import UIKit
import SVProgressHUD


/*
 SegueHandler implementation should be added to the Empfohlen
 https://bitbucket.org/snippets/alexandrzhovty/xn9G9
 */

//    MARK: - Properties & variables
final class LaunchController: UIViewController {
    //    MARK: Public
    
    
    //    MARK: Outlets
    @IBOutlet weak fileprivate var logoImageView: UIImageView!
    var imageProviders = Set<ImageProvider>()
    
    //    MARK: Private
    fileprivate var preferencesLoader: PreferencesLoader
    fileprivate var preferences: Preferences
    
    // MARK: Initialization
    required init?(coder aDecoder: NSCoder) {
        preferences = .default
//        preferencesLoader = PreferencesLoader(session: .shared, preferences: preferences)
        preferencesLoader = PreferencesLoader(for: preferences)
        super.init(coder: aDecoder)
        
    }
    
    deinit {
//        print("👍 ", String(describing: type(of: self)),":", #function)
        imageProviders.forEach{ $0.cancel() }
        imageProviders.removeAll()
    }
    
}

//    MARK: - View life cycle
extension LaunchController  {
    override func viewDidLoad() {
        super.viewDidLoad()
        Appearance.customize(viewController: self)
        navigationItem.titleView?.isHidden = true
        self.logoImageView.image = nil
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        navigationController?.setNavigationBarHidden(true, animated: animated)
//    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        navigationController?.setNavigationBarHidden(false, animated: animated)
        
        DispatchQueue.once {
            [weak self] in
            self?.loadDataFromServer()
        }
        
        
    }
    
}


// MARK: - Utilities
extension LaunchController {
    // MARK: Server
    fileprivate func loadDataFromServer() {
        
        SVProgressHUD.show()
        
        preferencesLoader.load {
            [weak self] error in
            guard let `self` = self else { return }
            guard error == nil else {
                let completion = { self.goFurther() }
                DispatchQueue.main.async {
                    Alert.default.showError(message: error!.localizedDescription, onComplete: completion)
                }
                return
            }
            OperationQueue.main.addOperation { self.goFurther() }
        }
        
    }
    
    fileprivate func goFurther() {
        if let url = preferences.appLogo {
            let imageProvider = ImageProvider(imageURL: url) {
                [weak self] image in
                guard let `self` = self else { return }
                
                SVProgressHUD.dismiss(completion: {
                    OperationQueue.main.addOperation {
                        self.logoImageView.image = image
                        self.view.layoutIfNeeded()
                        let vc = Storyboard.posts.instantiateViewController(PostsListController.self)
                        self.navigationController?.setViewControllers([vc], animated: true)
                    }
                })
            }
            self.imageProviders.insert(imageProvider)
        } else {
            let vc = Storyboard.posts.instantiateViewController(PostsListController.self)
            self.navigationController?.setViewControllers([vc], animated: false)
        }
    }
}




//    MARK: - Outlet functions
extension LaunchController  {
    //    MARK: Buttons
    @IBAction func didTapTest(_ sender: Any) {
        executeSegue(.showPostList, sender: self)
        let vc = Storyboard.main.instantiateViewController(PostsListController.self)
        navigationController?.setViewControllers([vc], animated: true)
    }
    //    MARK: Gesture handlers
}

// MARK: - Navigation & SegueHandler protocol
extension LaunchController: SegueHandler {
    enum SegueType: String {
        case showPostList
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segueType(for: segue) {
        case .showPostList: break
        }
    }
}

// MARK: - Animation
extension LaunchController: LaunchAnimationSourceController {
    var sourceImageView: UIImageView {
        return logoImageView        
    }
    
    var destinationImageView: UIImageView {
        guard let titleView = navigationItem.titleView as? NavigationTitleView else {
            assertionFailure("⚠️ incorrect title view")
            return UIImageView()
        }
        return titleView.imageView
    }
}

