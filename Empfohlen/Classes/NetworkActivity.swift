//
//  NetworkActivity.swift
//  Empfohlen
//
//  Created by Aleksandr Zhovtyi on 12/11/17.
//  Copyright © 2017 Aleksandr Zhovtyi. All rights reserved.
//

import UIKit

struct NetworkActivity {
    private static var loadingCount = 0
    private static var lock = NSLock()
    
    static func started() {
        
        if Thread.isMainThread == false {
            DispatchQueue.main.async {
                NetworkActivity.started()
            }
            return
        }
        
        // Thread save
        lock.lock(); defer { lock.unlock() }
        
        // Increase counter
        if loadingCount == 0 {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        loadingCount += 1
    }
    
    static func finished() {
        
        if Thread.isMainThread == false {
            DispatchQueue.main.async {
                NetworkActivity.finished()
            }
            return
        }
        
        // Thread save
        lock.lock(); defer { lock.unlock() }
        
        
        // Decrease counter
        if loadingCount > 0 {
            loadingCount -= 1
        }
        if loadingCount == 0 {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
    }
}
