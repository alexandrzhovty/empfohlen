//
//  Resource.swift
//  Empfohlen
//
//  Created by Aleksandr Zhovtyi on 11/30/17.
//Copyright © 2017 Aleksandr Zhovtyi. All rights reserved.
//

/*
 https://bitbucket.org/snippets/alexandrzhovty/eLGqA/resources
 http://swiftandpainless.com/creating-a-smart-file-template/
 */

import UIKit

struct Resources {
    enum HTML {
        static let template = Bundle.main.url(forResource: "Template", withExtension: "html")!
        static let post = Bundle.main.url(forResource: "Post", withExtension: "html")!
    }
}


