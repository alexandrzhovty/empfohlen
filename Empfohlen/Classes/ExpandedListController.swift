//
//  ExpandedListController.swift
//  Empfohlen
//
//  Created by Aleksandr Zhovtyi on 12/13/17.
//Copyright © 2017 Aleksandr Zhovtyi. All rights reserved.
//

import UIKit
import CoreData
import SVProgressHUD

private struct ExpandedItem {
    let title: String?
    let body: String?
    init(title: String?, body: String?) {
        self.title = title
        self.body = body
    }
}

//    MARK: - Properties & variables
final class ExpandedListController: UIViewController {
    //    MARK: Public
    var managedObjectContext: NSManagedObjectContext!
    var menu: Menu!
    
    //    MARK: Outlets
	@IBOutlet weak var tableView: UITableView!
    
    //    MARK: Private
    fileprivate var repository: [ExpandedItem] = []
    fileprivate var expandedIndexPath: IndexPath? = IndexPath(row: 0, section: 0)
    
    //    MARK: Enums & Structures
}

//    MARK: - View life cycle
extension ExpandedListController  {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        assert(managedObjectContext != nil)
        assert(menu != nil)
        
        // Customize appearance
        Appearance.customize(viewController: self)
        
        view.subviews.forEach(setup)
        view.subviews.forEach(configure)
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(20)) {
            SVProgressHUD.show()
            DispatchQueue.global(qos: .background).async {
                [weak self] in
                guard let `self` = self else { return }
                
                self.repository = self.prepareRepository()
                DispatchQueue.main.async {
                    [weak self] in
                    self?.tableView.reloadData()
                    SVProgressHUD.dismiss()
                }
            }
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if SVProgressHUD.isVisible() {
            SVProgressHUD.dismiss()
        }
    }
}

//    MARK: - Utilities
extension ExpandedListController  {
    fileprivate func setup(_ viewToSetup: UIView) {
        switch viewToSetup {
        case tableView:
            tableView.estimatedRowHeight = 44
            tableView.rowHeight = UITableViewAutomaticDimension
   

        default: break
        }
        
    }
    
    fileprivate func configure(_ viewToConfig: UIView) {
        switch viewToConfig {
        case tableView:
            if let view = tableView.tableHeaderView as? ExpandedListHeaderView {
                view.title = menu.title
            }
            
            
        default: break
        }
        
    }
    
    fileprivate func prepareRepository() -> [ExpandedItem] {       
        return menu.contentItems.map{ ExpandedItem(title: $0["title"], body: $0["text"]) }
    }
}

// MARK: - Table view
extension ExpandedListController: UITableViewDataSource, UITableViewDelegate {
    // MARK: Datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repository.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(ExpandedListTableCell.self, for: indexPath)
        let item = repository[indexPath.row]
        cell.title = item.title
        cell.body = indexPath == expandedIndexPath ? item.body : nil
        cell.isBodyHidden = indexPath != expandedIndexPath
        
        return cell
    }
    
    
    // MARK: Selection
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        UIView.setAnimationsEnabled(false)
        tableView.beginUpdates()
        
        if indexPath == expandedIndexPath {
            expandedIndexPath = nil
            tableView.reloadRows(at: [indexPath], with: .none)
        } else {
            if expandedIndexPath == nil {
                expandedIndexPath = indexPath
                tableView.reloadRows(at: [indexPath], with: .none)
            } else {
                let old = expandedIndexPath!
                expandedIndexPath = indexPath
                tableView.reloadRows(at: [old, indexPath], with: .none)
                
            }
        }
        
        tableView.endUpdates()
//         UIView.setAnimationsEnabled(true)
        
        if expandedIndexPath?.row == repository.count - 1 {
            tableView.scrollToRow(at: expandedIndexPath!, at: .bottom, animated: true)
        } else if let ip = expandedIndexPath, tableView.indexPathsForVisibleRows?.contains(ip) == false {
            tableView.scrollToRow(at: ip, at: .middle, animated: true)
        }
    }
}

