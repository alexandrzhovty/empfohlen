//
//  ImageProvider.swift
//  Famous
//
//  Created by Aleksandr Zhovtyi on 10/27/17.
//  Copyright © 2017 Empfohlen. All rights reserved.
//

import Foundation

import UIKit
class ImageProvider {
    
    private var operationQueue = OperationQueue()
    let imageURL: URL
    var operations: [Operation]!
    
    deinit {
        cancel()
    }
    
    init(imageURL: URL, completion: @escaping (Image?) -> ()) {
        
        self.imageURL = imageURL
        
        if let image = ImageCache.default.image(forKey: imageURL) {
            // There is nothing to do
            completion(image)
            return
        }
        
        
        
        operationQueue.maxConcurrentOperationCount = 5
        
       
        
        // Создаем операции
        let local = ImageLocalOperation(url: imageURL)
        let dataLoad = ImageRemoteOperation(url: imageURL)
//        let filter = Filter(image: nil)
        let output = ImageOutputOperation(completion: completion)
        
        
        
//        let operations = [dataLoad, filter, output]
        
        let operations = [local, dataLoad, output]
        self.operations = operations
        self.queuePriority = Operation.QueuePriority.normal
        
//        self.operationQueue
        
        // Добавляем dependencies
//        filter.addDependency(dataLoad)
//        output.addDependency(filter)
        dataLoad.addDependency(local)
        output.addDependency(dataLoad)
        
        operationQueue.addOperations(operations, waitUntilFinished: false)
        
        
    }
    
    var queuePriority: Operation.QueuePriority {
        get { return operations.first?.queuePriority ?? Operation.QueuePriority.veryLow }
        set { operations.forEach{ $0.queuePriority = newValue  }  }
    }
    
    func cancel() {
        operationQueue.cancelAllOperations()
    }
    
}

extension ImageProvider: Hashable {
    var hashValue: Int {
        return (imageURL.absoluteString).hashValue
    }
}

func ==(lhs: ImageProvider, rhs: ImageProvider) -> Bool {
    return lhs.imageURL == rhs.imageURL
}

