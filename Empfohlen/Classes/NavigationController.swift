//
//  NavigationController.swift
//  Empfohlen
//
//  Created by Aleksandr Zhovtyi on 11/29/17.
//  Copyright © 2017 Aleksandr Zhovtyi. All rights reserved.
//

import UIKit

extension UINavigationController {
    open override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
