//
//  ExpandedListHeaderView.swift
//  Empfohlen
//
//  Created by Aleksandr Zhovtyi on 12/13/17.
//  Copyright © 2017 Aleksandr Zhovtyi. All rights reserved.
//

import UIKit

protocol ExpandedListHeaderViewInterface {
    var title: String? { get set }
}

class ExpandedListHeaderView: UIView {
    @IBOutlet weak fileprivate var titleLabel: UILabel!

}

extension ExpandedListHeaderView: ExpandedListHeaderViewInterface {
    var title: String? {
        get { return titleLabel.text }
        set { titleLabel.text = newValue }
    }
}
