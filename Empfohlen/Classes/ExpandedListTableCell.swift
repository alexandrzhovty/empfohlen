//
//  ExpandedListTableCell.swift
//  Empfohlen
//
//  Created by Aleksandr Zhovtyi on 12/13/17.
//  Copyright © 2017 Aleksandr Zhovtyi. All rights reserved.
//

import UIKit

protocol ExpandedListViewInterface {
    var title: String? { get set }
    var body: String? { get set }
    
    var isBodyHidden: Bool { get set }
}

class ExpandedListTableCell: UITableViewCell {
    
    @IBOutlet weak fileprivate var titleLabel: UILabel!
    @IBOutlet weak fileprivate var bodyLabel: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}

extension ExpandedListTableCell: ExpandedListViewInterface {
    var isBodyHidden: Bool {
        get { return bodyLabel.isHidden  }
        set { bodyLabel.isHidden = newValue }
    }
    
    var title: String? {
        get { return titleLabel.text }
        set { titleLabel.text = newValue }
    }
    var body: String? {
        get { return bodyLabel.text }
        set {
            
            let options: [NSAttributedString.DocumentReadingOptionKey : Any] = [
                .documentType: NSAttributedString.DocumentType.html,
                .characterEncoding: String.Encoding.utf8.rawValue
            ]
            
            guard
                let str = newValue,
                let data = str.data(using: .utf8),
                let attributedString = try? NSAttributedString(data: data, options: options, documentAttributes: nil)
            else {
                bodyLabel.text = newValue
                return
            }
            
            let mutAttrString = NSMutableAttributedString(string: "", attributes: [NSAttributedStringKey.font : bodyLabel.font!])
            mutAttrString.append(attributedString)
            mutAttrString.addAttribute(NSAttributedStringKey.font, value: bodyLabel.font!, range: NSMakeRange(0, mutAttrString.length))
            
            
            let paragraph = NSMutableParagraphStyle()
            paragraph.paragraphSpacing = 30
//            paragraph.firstLineHeadIndent = 20
            mutAttrString.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraph, range: NSMakeRange(0, mutAttrString.length))
            
            
            bodyLabel.attributedText = mutAttrString
            
        }
    }
}

