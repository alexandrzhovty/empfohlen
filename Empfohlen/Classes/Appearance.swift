//
//  Appearance.swift
//  Empfohlen
//
//  Created by Aleksandr Zhovtyi on 11/29/17.
//Copyright © 2017 Aleksandr Zhovtyi. All rights reserved.
//

import UIKit
import SVProgressHUD

class Appearance {
    
    //    MARK: - Class functions
    class func customize() {
        
        UIApplication.shared.statusBarStyle = .lightContent
        UIApplication.shared.keyWindow?.tintColor  = UIColor.tint
        Appearance.customizeNavigationBar()
        Appearance.customizeProgressView()
        Appearance.customizeProgressHUD()
        Appearance.customizeRefreshControl()
    
    }
    
    class func customize(viewController: UIViewController) {
        //        let backButton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        //        viewController.navigationItem.leftBarButtonItem = backButton
        viewController.navigationController?.navigationBar.backItem?.title = " "
        viewController.navigationItem.titleView = NavigationTitleView()
    }
}

extension Appearance {
    static func customizeRefreshControl() {
        let refresh = UIRefreshControl.appearance()
        refresh.tintColor = UIColor.tint
    }
}


// MARK: - Navigation
extension Appearance {
    class func customizeNavigationBar() {
        let navBar = UINavigationBar.appearance()
        
        
        navBar.isTranslucent = false
        navBar.setBackgroundImage(UIImage(color: Color.Background.navigation)!, for: .default)
        navBar.shadowImage = UIImage(color: .clear)
        navBar.tintColor = .white

    }
}

// MARK: - Progress view
extension Appearance {
    static func customizeProgressView() {
        let progressView = UIProgressView.appearance()
        progressView.tintColor = Color(.richRed)
    }
}

// MARK: - Progreess HUD
extension Appearance {
    static func customizeProgressHUD() {
        let hud = SVProgressHUD.appearance()
        
        hud.defaultStyle = .custom
        hud.backgroundColor = Color.tint
        hud.foregroundColor = Color(.richRed)
    }
}
