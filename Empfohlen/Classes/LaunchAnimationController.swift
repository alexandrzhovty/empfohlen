//
//  LaunchAnimationController.swift
//  Empfohlen
//
//  Created by Aleksandr Zhovtyi on 11/29/17.
//  Copyright © 2017 Aleksandr Zhovtyi. All rights reserved.
//

import UIKit

protocol LaunchAnimationSourceController: class {
    var sourceImageView: UIImageView { get }
    var destinationImageView: UIImageView { get }
}

protocol LaunchAnimationDestinationController: class {
    
}

class LaunchAnimationController: NSObject {
    
}

extension LaunchAnimationController: UIViewControllerAnimatedTransitioning {
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.5
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let containerView = transitionContext.containerView
        let toViewController = transitionContext.viewController(forKey: .to)! as! PostsListController
        let fromViewController = transitionContext.viewController(forKey: .from) as! LaunchController
        let toView = toViewController.view
        let fromView = fromViewController.view
        
       
        
        containerView.addSubview(toView!)
        
        let dimmmingView = UIView(frame: containerView.bounds)
        dimmmingView.backgroundColor = Color.tint
        containerView.addSubview(dimmmingView)
        
        let imageView = fromViewController.sourceImageView
        let position = containerView.convert(imageView.center, from: imageView.superview)
        
        let newImageView = UIImageView(image: imageView.image)
        newImageView.frame = imageView.frame
        
        newImageView.contentMode = .scaleAspectFit
        
        AppDelegate.shared.window?.addSubview(newImageView)
        
//        containerView.addSubview(newImageView)
        imageView.center = position
        
        let end: (size: CGSize, center: CGPoint) = {
            let endPosition = containerView.convert(fromViewController.destinationImageView.center, from: fromViewController.destinationImageView.superview)
            let endSize = fromViewController.destinationImageView.frame.size
            return (endSize, endPosition)
        }()
        
        
        toViewController.destinationImageView.isHidden = true
        
        
        let duration = self.transitionDuration(using: transitionContext)
        UIView.animateKeyframes(withDuration: duration, delay: 0, options: [], animations: {
            
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 1, animations: {
                newImageView.frame.size = end.size
                newImageView.center = end.center
            })
            
            UIView.addKeyframe(withRelativeStartTime: 2/3, relativeDuration: 1/3, animations: {
                dimmmingView.alpha = 0
            })
            
            
        }) {
            finished in
            if (transitionContext.transitionWasCancelled) {
                toView?.removeFromSuperview()
            } else {
                fromView?.removeFromSuperview()
                fromView?.alpha = 1
            }
            
            toViewController.destinationImageView.isHidden = false
            dimmmingView.removeFromSuperview()
            newImageView.removeFromSuperview()
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }
    }
}
